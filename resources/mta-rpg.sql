-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 09 Gru 2018, 22:17
-- Wersja serwera: 10.1.37-MariaDB
-- Wersja PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `mtadm`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `admin_rights`
--

CREATE TABLE `admin_rights` (
  `id` int(10) NOT NULL,
  `globalId` int(10) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `admin_rights`
--

INSERT INTO `admin_rights` (`id`, `globalId`, `name`) VALUES
(1, 1, 'aprzedmiot.stworz'),
(2, 1, 'aprzedmiot.edytuj');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `characters`
--

CREATE TABLE `characters` (
  `id` int(10) NOT NULL,
  `globalId` int(10) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `skin` int(4) NOT NULL,
  `playTime` int(20) NOT NULL,
  `money` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `characters`
--

INSERT INTO `characters` (`id`, `globalId`, `name`, `skin`, `playTime`, `money`) VALUES
(1, 1, 'Vincent Dabrasco', 47, 35097, 1050),
(2, 3, 'Tommy Vegas', 0, 4305, 0),
(3, 2, 'Antonio Borletti', 12, 1951, 0),
(4, 4, 'Armani Brucio', 178, 27968, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `globals`
--

CREATE TABLE `globals` (
  `id` int(10) NOT NULL,
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `pass` varchar(40) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `adminLevel` int(4) NOT NULL,
  `score` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `globals`
--

INSERT INTO `globals` (`id`, `name`, `pass`, `adminLevel`, `score`) VALUES
(1, 'Hifi', 'df889f67e7c01711b9341ddfcecdf662', 3, 2000),
(2, 'Test', '098f6bcd4621d373cade4e832627b4f6', 3, 0),
(3, 'Bartman', 'cc03e747a6afbbcbf8be7668acfebee5', 3, 0),
(4, 'Beezy', 'cc03e747a6afbbcbf8be7668acfebee5', 3, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 NOT NULL,
  `cash` int(11) NOT NULL,
  `type` smallint(2) NOT NULL,
  `owner` int(11) NOT NULL,
  `value1` mediumint(6) NOT NULL,
  `value2` mediumint(6) NOT NULL,
  `color` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT 'FFFFFF',
  `tag` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'NONE',
  `flags` int(11) NOT NULL,
  `advertise` varchar(64) CHARACTER SET utf8 NOT NULL,
  `date` int(11) NOT NULL,
  `ooc` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `groups`
--

INSERT INTO `groups` (`id`, `name`, `cash`, `type`, `owner`, `value1`, `value2`, `color`, `tag`, `flags`, `advertise`, `date`, `ooc`) VALUES
(1, 'Los Santos Police Department', 0, 0, 0, 0, 0, '003DC2', 'LSPD', 0, '', 0, 1),
(2, 'Druga grupa', 0, 0, 0, 0, 0, 'FFFFFF', 'NONE', 0, '', 0, 1),
(3, 'Mega Burger', 0, 0, 0, 0, 0, 'FFFFFF', 'NONE', 0, '', 0, 1),
(4, 'test', 0, 0, 0, 0, 0, 'FFFFFF', 'NONE', 0, '', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `group_members`
--

CREATE TABLE `group_members` (
  `id` int(5) NOT NULL,
  `charId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `perm` smallint(3) NOT NULL,
  `title` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '-',
  `payment` int(11) NOT NULL,
  `skin` smallint(3) NOT NULL,
  `rank` smallint(3) NOT NULL DEFAULT '0',
  `payday` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `group_members`
--

INSERT INTO `group_members` (`id`, `charId`, `groupId`, `perm`, `title`, `payment`, `skin`, `rank`, `payday`) VALUES
(1, 3, 1, 0, 'Masakrator', 0, 189, 0, 0),
(2, 3, 2, 0, '-', 0, 0, 0, 0),
(3, 2, 1, 0, '-', 0, 0, 0, 0),
(4, 2, 2, 0, '-', 0, 0, 0, 0),
(5, 1, 1, 0, '-', 0, 0, 0, 0),
(6, 1, 2, 0, '-', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `ownerType` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `value1` int(10) NOT NULL,
  `value2` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `items`
--

INSERT INTO `items` (`id`, `ownerType`, `owner`, `type`, `name`, `value1`, `value2`) VALUES
(1, 1, 3, 1, 'iPhone', 621373, 1),
(2, 1, 3, 2, 'Zegarek', 1, 1),
(3, 1, 3, 3, 'Ubranie(182)', 182, 1),
(4, 1, 3, 4, 'DesertEagle', 24, 181),
(5, 1, 3, 5, 'NabojeDE', 24, 100),
(6, 1, 3, 5, 'ĄŚ', 24, 100),
(7, 1, 3, 5, 'NabojeDE', 24, 100),
(8, 1, 3, 5, 'NabojeDE', 24, 100),
(9, 1, 3, 5, 'NabojeDE', 24, 100),
(10, 1, 3, 5, 'NabojeDE', 24, 100),
(11, 1, 3, 5, 'NabojeDE', 24, 100),
(12, 1, 3, 6, 'Amfetamina', 10, 1),
(13, 1, 3, 6, 'Amfetamina', 10, 1),
(14, 1, 3, 6, 'Amfetamina', 10, 1),
(15, 1, 3, 6, 'Amfetamina', 10, 1),
(16, 1, 3, 6, 'Amfetamina', 10, 1),
(17, 1, 3, 6, 'Amfetamina', 10, 1),
(18, 1, 3, 6, 'Amfetamina', 10, 1),
(19, 1, 3, 6, 'Amfetamina', 10, 1),
(20, 1, 3, 6, 'Amfetamina', 10, 0),
(21, 1, 3, 6, 'Amfetamina', 10, 0),
(22, 1, 3, 6, 'Amfetamina', 10, 0),
(23, 1, 3, 6, 'Amfetamina', 10, 0),
(24, 1, 3, 6, 'Kokaina', 12, 1),
(25, 1, 3, 6, 'Kokaina', 12, 1),
(26, 1, 3, 6, 'Kokaina', 12, 1),
(27, 1, 3, 6, 'Kokaina', 12, 1),
(28, 1, 3, 6, 'Kokaina', 12, 1),
(29, 1, 3, 6, 'Kokaina', 12, 1),
(30, 1, 3, 6, 'Kokaina', 12, 1),
(31, 1, 3, 6, 'Kokaina', 12, 1),
(32, 1, 3, 6, 'Kokaina', 12, 1),
(33, 1, 3, 6, 'Kokaina', 12, 1),
(34, 1, 3, 6, 'Marihuana', 5, 1),
(35, 1, 3, 6, 'Marihuana', 5, 1),
(36, 1, 1, 1, 'Bron', 123, 10),
(37, 1, 1, 1, 'Deagle', 123, 10),
(38, 1, 1, 1, 'CosInnego', 123, 10),
(39, 1, 2, 1, 'Samsung', 123, 10),
(40, 1, 2, 1, 'iPhone', 123, 10),
(41, 1, 2, 1, 'TornaR1', 123, 10),
(42, 1, 2, 1, 'Kajdanki', 123, 10),
(43, 1, 2, 1, 'Amfetaminai', 123, 10),
(44, 1, 2, 1, 'Amfetaminai', 123, 10),
(45, 1, 2, 1, 'Amfetaminai', 123, 10),
(46, 1, 2, 1, 'Amfetaminai', 123, 10),
(47, 1, 2, 1, 'Amfetaminai', 123, 10),
(48, 1, 2, 1, 'Amfetaminai', 123, 10),
(49, 1, 2, 1, 'Amfetaminai', 123, 10),
(50, 1, 2, 1, 'Amfetaminai', 123, 10),
(51, 1, 2, 1, 'Amfetaminai', 123, 10),
(52, 1, 1, 1, 'Amfetaminai', 123, 10),
(53, 1, 1, 1, 'Amfetaminai', 123, 10),
(54, 1, 1, 1, 'Amfetaminai', 123, 10),
(55, 1, 1, 1, 'Amfetaminai', 123, 10),
(56, 1, 1, 1, 'Amfetaminai', 123, 10),
(57, 1, 1, 1, 'Amfetaminai', 123, 10),
(58, 1, 1, 1, 'Amfetaminai', 123, 10),
(59, 1, 1, 1, 'Amfetaminai', 123, 10),
(60, 1, 2, 1, 'JakisItemek', 50, 150),
(61, 1, 2, 1, 'JakisInny', 50, 150),
(62, 1, 2, 1, 'DesertEagle', 50, 150),
(63, 0, 0, 0, 'może teraz będzie działać', 0, 0),
(64, 1, 1, 1, '1', 1, 1),
(65, 1, 4, 1, 'Gangsta', 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `model` smallint(3) NOT NULL,
  `posX` float NOT NULL,
  `posY` float NOT NULL,
  `posZ` float NOT NULL,
  `rotX` float NOT NULL,
  `rotY` float NOT NULL,
  `rotZ` float NOT NULL,
  `dimension` int(10) NOT NULL DEFAULT '0',
  `interior` smallint(3) NOT NULL DEFAULT '0',
  `color` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '[[255,255,255,255,255,255,255,255,255,255,255,255]]',
  `fuel` float NOT NULL DEFAULT '5',
  `fuelType` tinyint(1) NOT NULL DEFAULT '1',
  `health` float NOT NULL DEFAULT '1000',
  `mileage` float NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '1',
  `visuals` varchar(256) CHARACTER SET utf8 NOT NULL DEFAULT '[{"wheels":[0,0,0,0],"doors":{"1":0,"2":0,"3":0,"4":0,"5":0,"0":0},"panels":{"1":0,"2":0,"3":0,"4":0,"5":0,"6":0,"0":0}}]',
  `paintjob` tinyint(1) NOT NULL DEFAULT '3',
  `access` tinyint(2) NOT NULL DEFAULT '0',
  `blockwheel` mediumint(6) NOT NULL DEFAULT '0',
  `numberplate` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT 'P0P0P0P0',
  `owner` int(11) NOT NULL DEFAULT '0',
  `ownerType` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `vehicles`
--

INSERT INTO `vehicles` (`id`, `model`, `posX`, `posY`, `posZ`, `rotX`, `rotY`, `rotZ`, `dimension`, `interior`, `color`, `fuel`, `fuelType`, `health`, `mileage`, `locked`, `visuals`, `paintjob`, `access`, `blockwheel`, `numberplate`, `owner`, `ownerType`) VALUES
(38, 411, 2368.57, -31.188, 26.4844, 0, 0, 121.6, 0, 0, '[[255,255,255,255,255,255,255,255,255,255,255,255]]', 5, 1, 1000, 0, 1, '[{\"wheels\":[0,0,0,0],\"doors\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"0\":0},\"panels\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0,\"0\":0}}]', 3, 0, 0, 'P0P0P0P0', 0, 0),
(39, 411, 2360.32, -36.4979, 26.4844, 0, 0, 199.725, 0, 0, '[[255,255,255,255,255,255,255,255,255,255,255,255]]', 5, 1, 1000, 0, 1, '[{\"wheels\":[0,0,0,0],\"doors\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"0\":0},\"panels\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0,\"0\":0}}]', 3, 0, 0, 'P0P0P0P0', 0, 0),
(40, 411, 2364.79, -33.5433, 26.4844, 0, 0, 360.441, 0, 0, '[[255,255,255,255,255,255,255,255,255,255,255,255]]', 5, 1, 1000, 0, 1, '[{\"wheels\":[0,0,0,0],\"doors\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"0\":0},\"panels\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0,\"0\":0}}]', 3, 0, 0, 'P0P0P0P0', 0, 0),
(41, 411, 2357.92, -29.5034, 26.3358, 0, 0, 125.835, 0, 0, '[[255,255,255,255,255,255,255,255,255,255,255,255]]', 5, 1, 1000, 0, 1, '[{\"wheels\":[0,0,0,0],\"doors\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"0\":0},\"panels\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0,\"0\":0}}]', 3, 0, 0, 'P0P0P0P0', 0, 0),
(42, 411, 2363.13, -30.7368, 26.3368, 0, 0, 373.641, 0, 0, '[[255,255,255,255,255,255,255,255,255,255,255,255]]', 5, 1, 1000, 11693, 1, '[{\"wheels\":[0,0,0,0],\"doors\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"0\":0},\"panels\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0,\"0\":0}}]', 3, 0, 0, 'P0P0P0P0', 1, 1),
(43, 560, 2384.98, 91.0118, 26.4473, 0, 0, 266.385, 0, 0, '[[255,255,255,255,255,255,255,255,255,255,255,255]]', 5, 1, 1000, 7080, 1, '[{\"wheels\":[0,0,0,0],\"doors\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"0\":0},\"panels\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0,\"0\":0}}]', 3, 0, 0, 'P0P0P0P0', 0, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `admin_rights`
--
ALTER TABLE `admin_rights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `global_idx` (`globalId`);

--
-- Indeksy dla tabeli `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `global_idx` (`globalId`),
  ADD KEY `globall_idx` (`globalId`);

--
-- Indeksy dla tabeli `globals`
--
ALTER TABLE `globals`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `group_members`
--
ALTER TABLE `group_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `char_uid` (`charId`),
  ADD KEY `group_idx` (`groupId`);

--
-- Indeksy dla tabeli `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`,`ownerType`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `admin_rights`
--
ALTER TABLE `admin_rights`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `globals`
--
ALTER TABLE `globals`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `group_members`
--
ALTER TABLE `group_members`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT dla tabeli `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `admin_rights`
--
ALTER TABLE `admin_rights`
  ADD CONSTRAINT `global` FOREIGN KEY (`globalId`) REFERENCES `globals` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `characters`
--
ALTER TABLE `characters`
  ADD CONSTRAINT `globall` FOREIGN KEY (`globalId`) REFERENCES `globals` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `group_members`
--
ALTER TABLE `group_members`
  ADD CONSTRAINT `character` FOREIGN KEY (`charId`) REFERENCES `characters` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `group` FOREIGN KEY (`groupId`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
