veh_lastpos = {}
veh_mileage = {}

lastSendTicks = 0

local GUI = {
    label = {}
}

vehicleKolor = nil

function onPress(button, press)
	local seat = getPedOccupiedVehicleSeat(localPlayer)
	if seat == false or seat > 0 then return end

	if press then
		if button == "k" then
			triggerServerEvent("onClientVehicleAction", resourceRoot, "start_engine")
		end

		if button == "lalt" then
			triggerServerEvent("onClientVehicleAction", resourceRoot, "lights_on")
		end

		if button == "s" or button == "a" or button == "d" then
			if not getVehicleEngineState(getPedOccupiedVehicle(localPlayer)) then
				cancelEvent()
			end
		end
	end
end
addEventHandler("onClientKey", root, onPress)

addEventHandler( "onClientResourceStart", resourceRoot,
    function ()
        setTimer(mileage, 1000, 0)
        setWorldSoundEnabled(19, 37, false)
    end
);

function mileage()
	local vehicle = getPedOccupiedVehicle(localPlayer)
	local seat = getPedOccupiedVehicleSeat(localPlayer)

	if isElement(vehicle) and seat == 0 and getVehicleEngineState(vehicle) then
		-- obliczamy przebieg
		if veh_lastpos[vehicle] == nil then
			-- ustawiamy po raz pierwszy last pos dla tego pojazdu
			veh_lastpos[vehicle] = {}
			veh_lastpos[vehicle][1], veh_lastpos[vehicle][2], veh_lastpos[vehicle][3] = getElementPosition(vehicle)
		else
			local x, y, z = getElementPosition(vehicle)
			local distance = getDistanceBetweenPoints3D(veh_lastpos[vehicle][1], veh_lastpos[vehicle][2], veh_lastpos[vehicle][3], x, y, z)
			veh_mileage[vehicle] = veh_mileage[vehicle] + math.floor(distance)

			veh_lastpos[vehicle][1], veh_lastpos[vehicle][2], veh_lastpos[vehicle][3] = x, y, z

			lastSendTicks = lastSendTicks + 1

			-- co 5 sekund
			if lastSendTicks >= 10 then
				lastSendTicks = 0
				triggerServerEvent("updateVehicleMileage", resourceRoot, vehicle, veh_mileage[vehicle])
			end
		end
	end
end

addEventHandler("onClientVehicleEnter", getRootElement(),
    function(thePlayer, seat)
        local type = getVehicleType(source)
        if thePlayer == localPlayer then
            if seat == 0 then
                veh_mileage[source] = getElementData(source, "vehicle:mileage") or 0
            end
        end
    end
)
addEventHandler("onClientVehicleExit", getRootElement(),
    function(thePlayer, seat)
        local type = getVehicleType(source)
        if thePlayer == localPlayer then
            if seat == 0 then
                triggerServerEvent("updateVehicleMileage", resourceRoot, source, veh_mileage[source])
                veh_lastpos[source] = nil
                veh_mileage[source] = nil
            end
        end
    end
)

addEventHandler( "onClientResourceStop", resourceRoot,
    function ()
        local vehicle = getPedOccupiedVehicle(localPlayer)
		local seat = getPedOccupiedVehicleSeat(localPlayer)

		if isElement(vehicle) and seat == 0 then
			triggerServerEvent("updateVehicleMileage", resourceRoot, vehicle, veh_mileage[vehicle])
		end
    end
);

addEvent("playVehicleLockSound", true)
addEventHandler("playVehicleLockSound", resourceRoot,
	function(x, y, z)
		local px, py, pz = getElementPosition(localPlayer)
		if getDistanceBetweenPoints3D(x, y, z, px, py, pz) < 30 then
			playSound3D("files/car_lock.mp3", x, y, z)
		end
	end
)


--- SOME ACMD STUFF

addEventHandler("onClientResourceStart", resourceRoot,
    function()
        GUI.window = guiCreateWindow(0.42, 0.41, 0.16, 0.14, "Przypisywanie pojazdu", true)
        guiWindowSetMovable(GUI.window, false)
        guiWindowSetSizable(GUI.window, false)

        GUI.ownertype = guiCreateComboBox(0.37, 0.22, 0.59, 0.62, "Gracz", true, GUI.window)
        GUI.label[1] = guiCreateLabel(0.07, 0.22, 0.28, 0.13, "Typ właściciela", true, GUI.window)
        guiLabelSetVerticalAlign(GUI.label[1], "center")
        GUI.label[2] = guiCreateLabel(0.07, 0.44, 0.28, 0.13, "UID właściciela", true, GUI.window)
        guiLabelSetVerticalAlign(GUI.label[2], "center")
        GUI.owner = guiCreateEdit(0.37, 0.44, 0.59, 0.13, "", true, GUI.window)
        GUI.button = guiCreateButton(0.08, 0.67, 0.83, 0.19, "Przypisz", true, GUI.window)

        local w = guiGetSize(GUI.window, false)
        local width = dxGetTextWidth("zamknij", 1, "default")
        local label = guiCreateLabel("right" == "left" and 5 or w - 5 - width, 2, width, 15, "zamknij", false, GUI.window)
        
        guiLabelSetColor(label, 160, 160, 160)
        guiLabelSetHorizontalAlign(label, "center", false)
        guiSetProperty(label, "ClippedByParent", "False")
        guiSetProperty(label, "AlwaysOnTop", "True")

        addEventHandler("onClientGUIClick", label, 
            function(button, state)
                if button == "left" and state == "up" then
                    -- back
                    guiSetVisible(GUI.window, false)
                    showCursor(false)
                end
            end, 
        false)

        addEventHandler("onClientMouseEnter", label, function() guiLabelSetColor(label, 255, 69, 59) end, false)
        addEventHandler("onClientMouseLeave", label, function() guiLabelSetColor(label, 160, 160, 160) end, false)

        addEventHandler("onClientGUIClick", GUI.button, 
            function(button, state)
                if button == "left" and state == "up" then
                    -- back
                    guiSetVisible(GUI.window, false)
                    showCursor(false)

                    triggerServerEvent("avehPrzypiszResponse", resourceRoot, guiComboBoxGetSelected(GUI.ownertype)+1, tonumber(guiGetText(GUI.owner)))
                end
            end, 
        false)

        guiSetVisible(GUI.window, false)
    end
)

function showAvehPrzypisz(ownertypes)
	guiSetText(GUI.owner, "")
	guiComboBoxClear(GUI.ownertype)
	for k,v in ipairs(ownertypes) do
		guiComboBoxAddItem(GUI.ownertype, v)
	end

	guiComboBoxSetSelected(GUI.ownertype, 0)

	guiSetVisible(GUI.window, true)
	showCursor(true)
	setCursorAlpha(255)
end

addEvent("showAvehPrzypisz", true)
addEventHandler("showAvehPrzypisz", resourceRoot, showAvehPrzypisz)

function showAvehKolor(vehicle)
	local colors = { getVehicleColor(vehicle, true) }
	vehicleKolor = vehicle

	showCursor(true)
	colorPicker.openSelect(colors)
end

addEvent("showAvehKolor", true)
addEventHandler("showAvehKolor", resourceRoot, showAvehKolor)

function closedColorPicker()
	local r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4 = getVehicleColor(vehicleKolor, true)

	triggerServerEvent("avehKolorResponse", resourceRoot, vehicleKolor, r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4)

	vehicleKolor = nil
	showCursor(false)
end

function updateColor()
	if (not colorPicker.isSelectOpen) then return end
	local r, g, b = colorPicker.updateTempColors()
	if (vehicleKolor and isElement(vehicleKolor)) then
		local r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4  = getVehicleColor(vehicleKolor, true)
		if (guiCheckBoxGetSelected(checkColor1)) then
			r1, g1, b1 = r, g, b
		end
		if (guiCheckBoxGetSelected(checkColor2)) then
			r2, g2, b2 = r, g, b
		end
		if (guiCheckBoxGetSelected(checkColor3)) then
			r3, g3, b3 = r, g, b
		end
		if (guiCheckBoxGetSelected(checkColor4)) then
			r4, g4, b4 = r, g, b
		end
		setVehicleColor(vehicleKolor, r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4)
	end
end
addEventHandler("onClientRender", root, updateColor)

---