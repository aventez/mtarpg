local screenW, screenH = guiGetScreenSize()

local width,height = 0.223 * screenW, 0.398 * screenH
local tmp = (width + height) / 2
width, height = tmp, tmp

local marginw, marginh = width + 0.0156 * screenW, height - 0.0462 * screenH

addEventHandler("onClientVehicleEnter", getRootElement(),
    function(thePlayer, seat)
        local type = getVehicleType(source)
        if type == "Bike" or type == "BMX" then return end
        if thePlayer == localPlayer then
            if seat == 0 then
                addEventHandler("onClientRender", root, renderSpeedo)
            end
        end
    end
)
addEventHandler("onClientVehicleExit", getRootElement(),
    function(thePlayer, seat)
        local type = getVehicleType(source)
        if type == "Bike" or type == "BMX" then return end
        if thePlayer == localPlayer then
            if seat == 0 then
                removeEventHandler("onClientRender", root, renderSpeedo)
            end
        end
    end
)


function renderSpeedo()
    local vehicle = getPedOccupiedVehicle(localPlayer)
    if vehicle then
        dxDrawImage(screenW - marginw, screenH - marginh, width, height, "files/speedo/speedo.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
        dxDrawImage(screenW - marginw, screenH - marginh, width, height, "files/speedo/indicator.png", getElementSpeed(vehicle, "km/h"), 0, 0, tocolor(255, 255, 255, 255), false)
        dxDrawImage(screenW - marginw, screenH - marginh, width, height, "files/speedo/distance.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)

        if getVehicleEngineState(vehicle) then
            local total_mileage = (veh_mileage[vehicle] or 0)
            local km = math.floor(total_mileage/1000)
            local m10 = math.floor(total_mileage/100) - km*10
            local strkm = tostring(km)

            if #strkm < 6 then
                local prepend = "" 
                for i=1, 6-#strkm do
                    prepend = prepend .. "0"
                end
                strkm = prepend .. strkm
            end

            for i = 1, #strkm do
                local c = strkm:sub(i,i)
                dxDrawText(c, screenW - (0.43913043 * marginw + (0.0354347 * marginw * (7-i))), screenH - 0.26315789 * marginh, screenW - (0.43913043 * marginw + (0.0354347 * marginw * (7-i))) + 0.0369565 * marginw, screenH - 0.18936842 * marginh, tocolor(255, 255, 255, 255), 1, "default", "center", "center", false, false, false, false, true)
            end
            
            dxDrawText(m10, screenW - 0.43913043 * marginw, screenH - 0.26315789 * marginh, screenW - 0.40282608 * marginw, screenH - 0.18936842 * marginh, tocolor(0, 0, 0, 255), 1, "default", "center", "center", false, false, false, false, true)
        end
    end
end


function getElementSpeed(theElement, unit)
    -- Check arguments for errors
    assert(isElement(theElement), "Bad argument 1 @ getElementSpeed (element expected, got " .. type(theElement) .. ")")
    local elementType = getElementType(theElement)
    assert(elementType == "player" or elementType == "ped" or elementType == "object" or elementType == "vehicle" or elementType == "projectile", "Invalid element type @ getElementSpeed (player/ped/object/vehicle/projectile expected, got " .. elementType .. ")")
    assert((unit == nil or type(unit) == "string" or type(unit) == "number") and (unit == nil or (tonumber(unit) and (tonumber(unit) == 0 or tonumber(unit) == 1 or tonumber(unit) == 2)) or unit == "m/s" or unit == "km/h" or unit == "mph"), "Bad argument 2 @ getElementSpeed (invalid speed unit)")
    -- Default to m/s if no unit specified and 'ignore' argument type if the string contains a number
    unit = unit == nil and 0 or ((not tonumber(unit)) and unit or tonumber(unit))
    -- Setup our multiplier to convert the velocity to the specified unit
    local mult = (unit == 0 or unit == "m/s") and 50 or ((unit == 1 or unit == "km/h") and 180 or 111.84681456)
    -- Return the speed by calculating the length of the velocity vector, after converting the velocity to the specified unit
    return (Vector3(getElementVelocity(theElement)) * mult).length
end