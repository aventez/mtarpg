local screenW, screenH = guiGetScreenSize()
local vehiclesList = {
	['font'] = dxCreateFont("files/list/myriadproregular.ttf", 14, false, "cleartype"),
	['pos'] = {['x'] = screenW/2 - 256, ['y'] = screenH * 0.4 - 128},
	['visible'] = false
}

local vehicleTrack = {
	['enabled'] = false
}

local actionsTitle = {["spawn"] = "Spawn", ["unspawn"] = "Unspawn", ["track"] = "Namierz", ["close"] = "Zamknij"}

function showVehiclesList(vehicles)
	vehiclesList['data'] = vehicles

	-- prepare actions for each vehicle
	for k,v in pairs(vehiclesList['data']) do
		if v['is_spawned'] then
			vehiclesList['data'][k]['actions'] = {"unspawn", "track", "close"}
		else
			vehiclesList['data'][k]['actions'] = {"spawn", "close"}
		end
	end

	vehiclesList['selected'] = 1
	vehiclesList['action'] = 1
	vehiclesList['visible'] = true

	addEventHandler("onClientRender", root, renderVehiclesList)

	showCursor(true)
	setCursorAlpha(0)
end

addEvent("showVehiclesList", true)
addEventHandler("showVehiclesList", resourceRoot, showVehiclesList)

function hideVehiclesList()
	vehiclesList['data'] = nil
	vehiclesList['visible'] = false

	showCursor(false)
	setCursorAlpha(255)

	removeEventHandler("onClientRender", root, renderVehiclesList)
end


function renderVehiclesList()
	local selected_veh = vehiclesList['selected']

	local veh_img = "files/vehicles/1.jpg"
	if fileExists(string.format(":cstory_vehiclescore/files/vehicles/%d.jpg", vehiclesList['data'][selected_veh]['model'])) then
		veh_img = string.format("files/vehicles/%d.jpg", vehiclesList['data'][selected_veh]['model'])
	end

	dxDrawImage(vehiclesList['pos']['x'], vehiclesList['pos']['y'], 512, 256, "files/list/bg.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
	dxDrawImage(vehiclesList['pos']['x'] + 50, vehiclesList['pos']['y'] + 25, 256, 128, veh_img, 0, 0, 0, tocolor(255, 255, 255, 255), false)

	dxDrawText(getVehicleNameFromModel(vehiclesList['data'][selected_veh]['model']), vehiclesList['pos']['x'] + 60, vehiclesList['pos']['y'] + 155, vehiclesList['pos']['x'] + 60, vehiclesList['pos']['y'] + 155, tocolor(57, 161, 224, 255), 1, vehiclesList['font'], "left", "top", false, false, false, false, true)
	dxDrawText(string.format("ID: %d", vehiclesList['data'][selected_veh]['id']), vehiclesList['pos']['x'] + 50, vehiclesList['pos']['y'] + 160, vehiclesList['pos']['x'] + 306, vehiclesList['pos']['y'] + 160, tocolor(255, 255, 255, 255), 0.5, vehiclesList['font'], "right", "top", false, false, false, false, true)
	-- szczegoly
	local info = nil
	info = string.format("Przebieg: %.1fkm\nSpalanie: 0.0l/100km\nW baku: 0.0l\nStan: %d%%", (vehiclesList['data'][selected_veh]['mileage']/1000), math.ceil(vehiclesList['data'][selected_veh]['health']/10))
	-- "Przebieg: 1123.4km\nSpalanie: 34.1l/100km\nW baku: 28.1l\nStan: 100%"
	dxDrawText(info, vehiclesList['pos']['x'] + 50, vehiclesList['pos']['y'] + 178, vehiclesList['pos']['x'] + 306, vehiclesList['pos']['y'] + 280, tocolor(255, 255, 255, 255), 0.58, vehiclesList['font'], "left", "top", false, false, false, false, true)

	dxDrawImage(vehiclesList['pos']['x'] + 326, vehiclesList['pos']['y'], 1, 256, "files/list/line.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)

	if vehiclesList['action'] < 1 then vehiclesList['action'] = 3 end
	if vehiclesList['action'] > 3 then vehiclesList['action'] = 1 end

	-- kolory przyciskow akcji (dla 3 max)
	local buttoncolor = {tocolor(255, 255, 255, 255), tocolor(255, 255, 255, 255), tocolor(255, 255, 255, 255)}
	buttoncolor[vehiclesList['action']] = tocolor(57, 161, 224, 255)

	-- przyciski
	for k,v in ipairs(vehiclesList['data'][selected_veh]['actions']) do
		local buttoncolor = tocolor(255, 255, 255, 255)
		if vehiclesList['action'] == k then buttoncolor = tocolor(57, 161, 224, 255) end

		dxDrawText(actionsTitle[v], vehiclesList['pos']['x'] + 336, vehiclesList['pos']['y'] + 55 + (k-1)*20, vehiclesList['pos']['x'] + 436, vehiclesList['pos']['y'] + 70 + (k-1)*20, buttoncolor, 0.7, vehiclesList['font'], "left", "center", false, false, false, false, true)
	end

	-- zmiana auta
	if vehiclesList['selected'] > 1 then
		dxDrawImage(vehiclesList['pos']['x'] - 32, vehiclesList['pos']['y'] + 112, 32, 32, "files/list/arrow_l.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
	end

	if #vehiclesList['data'] > vehiclesList['selected'] then
		dxDrawImage(vehiclesList['pos']['x'] + 512, vehiclesList['pos']['y'] + 112, 32, 32, "files/list/arrow_r.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
	end
end

function listOnPress(button, press)
	if press and vehiclesList['visible'] then
		if button == "arrow_u" then
			vehiclesList['action'] = vehiclesList['action'] - 1
			if vehiclesList['action'] < 1 then vehiclesList['action'] = #vehiclesList['data'][vehiclesList['selected']]['actions'] end
		end

		if button == "arrow_d" then
			vehiclesList['action'] = vehiclesList['action'] + 1
			if vehiclesList['action'] > #vehiclesList['data'][vehiclesList['selected']]['actions'] then vehiclesList['action'] = 1 end
		end

		if button == "arrow_r" and #vehiclesList['data'] > vehiclesList['selected'] then
			vehiclesList['selected'] = vehiclesList['selected'] + 1
			vehiclesList['action'] = 1
		end

		if button == "arrow_l" and vehiclesList['selected'] > 1 then
			vehiclesList['selected'] = vehiclesList['selected'] - 1
			vehiclesList['action'] = 1
		end

		if button == "escape" then
			hideVehiclesList()
			cancelEvent()
		end

		if button == "enter" then
			local selected_veh = vehiclesList['selected']
			local selected_action = vehiclesList['data'][selected_veh]['actions'][vehiclesList['action']]

			cancelEvent()

			if selected_action == "close" then
				-- nie wysylamy triggera ;)
			elseif selected_action == "track" then
				setTrackOnVehicle(vehiclesList['data'][selected_veh]['element'])
			else
				triggerServerEvent("onVehicleListResponse", resourceRoot, vehiclesList['data'][selected_veh]['id'], selected_action)
			end

			hideVehiclesList()
		end
	end
end
addEventHandler("onClientKey", root, listOnPress)

function setTrackOnVehicle(vehicle)
	if vehicleTrack['enabled'] then return exports.cstory_core:showInfoBox("Masz już włączone namierzanie pojazdu.\nUżyj /v namierz-off aby wyłączyć") end

	vehicleTrack['enabled'] = true
	vehicleTrack['blip'] = createBlipAttachedTo(vehicle, 55, 1, 0, 0, 0, 0, 32767, 65535)
	local x, y, z = getElementPosition(vehicle)
	vehicleTrack['marker'] = createMarker(x, y, z, "cylinder", 4, 0, 142, 243, 186)
	attachElements(vehicleTrack['marker'], vehicle, 0, 0, -1)

	exports.cstory_core:showInfoBox("Namierzanie zostało włączone, pojazd jest oznaczony na mapie\nAby wyłączyć namierzanie podejdź do auta lub użyj /v namierz-off")
end

addEventHandler("onClientMarkerHit", root,
		function (hitPlayer, matchingDimension)
			if hitPlayer == localPlayer and matchingDimension and source == vehicleTrack['marker'] then
				stopTrackOnVehicle()
			end
		end
	)

function stopTrackOnVehicle(from_command)
	if from_command and vehicleTrack['enabled'] then
		exports.cstory_core:showInfoBox("Namierzanie pojazdu zostało wyłączone")
	end
	destroyElement(vehicleTrack['blip'])
	destroyElement(vehicleTrack['marker'])

	vehicleTrack = {}
	vehicleTrack['enabled'] = false
end
addEvent("stopTrackVehicle", true)
addEventHandler("stopTrackVehicle", resourceRoot, stopTrackOnVehicle)