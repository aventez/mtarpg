function vehCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") then return end

	local arg = {...}

	local sub = arg[1] or ""

	if sub == "zamknij" or sub == "z" then
		local pretender, pretender_distance = nil, 5
		local x, y, z = getElementPosition(source)
		for k,v in ipairs(getElementsByType("vehicle")) do
			local vx, vy, vz = getElementPosition(v)
			local distance = getDistanceBetweenPoints3D(x, y, z, vx, vy, vz)
			if distance <= pretender_distance then
				pretender_distance = distance
				pretender = v
			end
		end

		if pretender then
			lockVehicleByPlayer(pretender, source)
		end
	elseif sub == "namierz-off" then
		triggerClientEvent(source, "stopTrackVehicle", resourceRoot, true)
	else
		local vehicles, rows = exports.cstory_db:queryResult("SELECT * FROM vehicles WHERE ownerType = ? AND owner = ?", VehiclesManager.OWNER_TYPE_PLAYER, getElementData(source, "character:char_id"))

		if not rows or rows == 0 then return exports.cstory_core:outputInfoBox("Nie posiadasz żadnych prywatnych pojazdów", source) end
		if rows == 1 then vehicles = {vehicles} end

		-- sprawdzamy czy jest zespawnowane autko
		for k,v in pairs(vehicles) do
			local veh = VehiclesManager.getVehicleByUid(v['id'])
			if veh then 
				vehicles[k]['is_spawned'] = true
				vehicles[k]['element'] = veh
			else vehicles[k]['is_spawned'] = false end
		end

		triggerClientEvent(source, "showVehiclesList", resourceRoot, vehicles)

		exports.cstory_core:outputCommandError("Tip: /(v)eh (z)amknij/namierz-off", source)
	end
end
addCommandHandler("veh", vehCommand)
addCommandHandler("v", vehCommand)


function lockVehicleByPlayer(vehicle, player)
	if not VehicleFunction.canPlayerUse(vehicle, player) then
		exports.cstory_core:outputInfoBox( "Nie posiadasz kluczy do tego pojazdu", player)
		return
	end
	VehicleFunction.changeLockState(vehicle)

	-- wysylamy playsound'a z otwierania auta do wszystkich
	triggerClientEvent("playVehicleLockSound", resourceRoot, getElementPosition(vehicle))
	setPedAnimation(player, "HEIST9", "Use_SwipeCard", 800, false, false, false, false)

	if Vehicles[vehicle]['locked'] then
		exports.cstory_core:outputInfoBox(string.format("Pojazd %s (UID: %d) został zamknięty", getVehicleName(vehicle), Vehicles[vehicle]['uid']), player)
	else
		exports.cstory_core:outputInfoBox(string.format("Pojazd %s (UID: %d) został otwarty", getVehicleName(vehicle), Vehicles[vehicle]['uid']), player)
	end
end


function onVehicleListResponse(vehicle_uid, action)
	if action == "spawn" then
		local vehicle = VehiclesManager.getVehicleByUid(vehicle_uid)

		if vehicle then return end

		local ntneed, vehicle = VehiclesManager.load(string.format(" WHERE id = %d", vehicle_uid))

		if ntneed == 0 then
			exports.cstory_core:outputCommandError("Pojazd o tym uid nie istnieje", client)
		else
			exports.cstory_core:outputInfoBox(string.format("Pomyślnie zespawnowano pojazd %s (UID: %d)", getVehicleName(vehicle), vehicle_uid), client)
		end
	elseif action == "unspawn" then
		local vehicle = VehiclesManager.getVehicleByUid(vehicle_uid)
		if not vehicle then return end

		local counter = 0

		for seat, player in pairs(getVehicleOccupants(vehicle)) do
		    counter = counter + 1
		end

		if counter > 0 then return exports.cstory_core:outputInfoBox("Nie można odspawnować pojazdu, ponieważ znajdują się w nim jakieś osoby", client) end

		exports.cstory_core:outputInfoBox(string.format("Pomyślnie odspawnowano pojazd %s (UID: %d)", getVehicleName(vehicle), vehicle_uid), client)
		VehiclesManager.unload(vehicle)
	end
end

addEvent("onVehicleListResponse", true)
addEventHandler("onVehicleListResponse", resourceRoot, onVehicleListResponse)