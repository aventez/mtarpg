VehicleFunction = {}


function VehicleFunction.changeLockState(vehicle)
	Vehicles[vehicle]['locked'] = not Vehicles[vehicle]['locked']
	setVehicleLocked(vehicle, Vehicles[vehicle]['locked'])
	setElementData(vehicle, 'vehicle:locked', Vehicles[vehicle]['locked'])
end

function VehicleFunction.changeEngineState(vehicle)
	Vehicles[vehicle]['engine'] = not Vehicles[vehicle]['engine']
	setVehicleEngineState(vehicle, Vehicles[vehicle]['engine'])

	setVehicleDamageProof(vehicle, not Vehicles[vehicle]['engine'])

	if not Vehicles[vehicle]['engine'] then VehiclesManager.save(vehicle) end
end

function VehicleFunction.changeLightsState(vehicle)
	Vehicles[vehicle]['lights'] = not Vehicles[vehicle]['lights']

	if Vehicles[vehicle]['lights'] then	setVehicleOverrideLights(vehicle, 2)
	else setVehicleOverrideLights(vehicle, 1) end
	
end

function VehicleFunction.canPlayerUse(vehicle, player)
	if not vehicle or not player then return false end
	if getElementType(vehicle) ~= "vehicle" then return false end
	if getElementType(player) ~= "player" then return false end
	if not getElementData(player, "character:char_id") then return false end
	if getElementData(player, "character:adminDuty") and getElementData(player, "global:member_admin_level") >= 2 then return true end

	if Vehicles[vehicle]['owner_type'] == VehiclesManager.OWNER_TYPE_PLAYER then
		if Vehicles[vehicle]['owner'] == getElementData(player, "character:char_id") then return true
		else return false end
	elseif Vehicles[vehicle]['owner_type'] == VehiclesManager.OWNER_TYPE_GROUP then

	end
end