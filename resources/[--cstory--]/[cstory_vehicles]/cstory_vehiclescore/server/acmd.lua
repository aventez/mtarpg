function aVehCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") or not getElementData(source, "character:adminDuty") or getElementData(source, "global:member_admin_level") < 2 then return end

	local arg = {...}

	local sub = arg[1] or ""

	if sub == "stworz" then
		local model = tonumber(arg[2]) or 0
		if model == 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh stworz [model pojazdu]", source) end

		local matrix = (source.matrix.position + source.matrix.forward * 3)
		local rx, ry, rz = getElementRotation(source)

		local vuid = VehiclesManager.create(model, matrix.x, matrix.y, matrix.z, 0, 0, rz+90, getElementDimension(source), getElementInterior(source))
		if vuid > 0 then		
			exports.cstory_core:outputInfoBox(string.format("Pomyślnie utworzono pojazd %s (UID: %d)\nZaleca się teraz przypisanie właściciela do pojazdu\noraz ustawienie rodzaju paliwa (domyślnie benzyna)", getVehicleNameFromModel(model), vuid), source)
		end

	elseif sub == "spawn" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh spawn [uid pojazdu]", source) end
		local vehicle = VehiclesManager.getVehicleByUid(vuid)

		if vehicle then return exports.cstory_core:outputCommandError("Pojazd o tym uid jest już zespawnowany", source) end

		local ntneed, vehicle = VehiclesManager.load(string.format(" WHERE id = %d", vuid))

		if ntneed == 0 then
			exports.cstory_core:outputCommandError("Pojazd o tym uid nie istnieje", source)
		else
			exports.cstory_core:outputInfoBox(string.format("Pomyślnie zespawnowano pojazd %s (UID: %d)", getVehicleName(vehicle), vuid), source)
		end
	elseif sub == "unspawn" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh unspawn [uid pojazdu]", source) end
		local vehicle = VehiclesManager.getVehicleByUid(vuid)

		if not vehicle then return exports.cstory_core:outputCommandError("Pojazd o tym uid nie jest zespawnowany", source) end

		exports.cstory_core:outputInfoBox(string.format("Pomyślnie odspawnowano pojazd %s (UID: %d)", getVehicleName(vehicle), vuid), source)
		VehiclesManager.unload(vehicle)
	elseif sub == "usun" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh usun [uid pojazdu]", source) end

		VehiclesManager.delete(vuid)
		exports.cstory_core:outputInfoBox(string.format("Pomyślnie usunięto pojazd o UID: %d", vuid), source)
	elseif sub == "nearuid" then
		local pretender, pretender_distance = nil, 40
		local x, y, z = getElementPosition(source)
		for k,v in ipairs(getElementsByType("vehicle")) do
			local vx, vy, vz = getElementPosition(v)
			local distance = getDistanceBetweenPoints3D(x, y, z, vx, vy, vz)
			if distance <= pretender_distance then
				pretender_distance = distance
				pretender = v
			end
		end

		if pretender then
			exports.cstory_core:outputInfoBox(string.format("Najbliższy pojazd to %s (UID: %d)", getVehicleName(pretender), Vehicles[pretender]['uid']), source)
		else 
			exports.cstory_core:outputCommandError("W pobliżu nie znajduje się żaden pojazd", source)
		end
	elseif sub == "zaparkuj" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh zaparkuj [uid pojazdu]", source) end
		local vehicle = VehiclesManager.getVehicleByUid(vuid)

		if not vehicle then return exports.cstory_core:outputCommandError("Pojazd o tym uid nie jest zespawnowany", source) end

		local x, y, z = getElementPosition(vehicle)
		local rx, ry, rz = getElementRotation(vehicle)
		local dimension, interior = getElementDimension(vehicle), getElementInterior(vehicle)

		exports.cstory_db:query("UPDATE vehicles SET posX = ?, posY = ?, posZ = ?, rotX = ?, rotY = ?, rotZ = ?, dimension = ?, interior = ? WHERE id = ?", x, y, z, rx, ry, rz, dimension, interior, vuid)
		Vehicles[vehicle]['posx'] = x
		Vehicles[vehicle]['posy'] = y
		Vehicles[vehicle]['posz'] = z
		Vehicles[vehicle]['rotx'] = rx
		Vehicles[vehicle]['roty'] = ry
		Vehicles[vehicle]['rotz'] = rz
		Vehicles[vehicle]['dimension'] = dimension
		Vehicles[vehicle]['interior'] = interior

		exports.cstory_core:outputInfoBox(string.format("Pojazd %s (UID: %d) został pomyślnie zaparkowany", getVehicleName(vehicle), vuid), source)
	elseif sub == "napraw" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh napraw [uid pojazdu]", source) end
		local vehicle = VehiclesManager.getVehicleByUid(vuid)

		if not vehicle then return exports.cstory_core:outputCommandError("Pojazd o tym uid nie jest zespawnowany", source) end

		setElementHealth(vehicle, 1000)

		for i=0,6 do
			if i <= 5 then
				setVehicleDoorState(vehicle, i, 0)
			end
			setVehiclePanelState(vehicle, i, 0)
		end
		setVehicleWheelStates(vehicle, 0, 0, 0, 0)

		VehiclesManager.save(vehicle)

		exports.cstory_core:outputInfoBox(string.format("Pojazd %s (UID: %d) został naprawiony", getVehicleName(vehicle), vuid), source)
	elseif sub == "przypisz" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh przypisz [uid pojazdu]", source) end
		local vehicle = VehiclesManager.getVehicleByUid(vuid)

		if not vehicle then return exports.cstory_core:outputCommandError("Pojazd o tym uid nie jest zespawnowany", source) end

		setElementData(source, "avehprzypisz_tmp", vehicle, false)
		triggerClientEvent(source, "showAvehPrzypisz", resourceRoot, VehiclesManager.ownertypes)
	elseif sub == "goto" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh przypisz [uid pojazdu]", source) end
		local vehicle = VehiclesManager.getVehicleByUid(vuid)

		if not vehicle then return exports.cstory_core:outputCommandError("Pojazd o tym uid nie jest zespawnowany", source) end

		setElementPosition(source, getElementPosition(vehicle))
		setElementInterior(source, getElementInterior(vehicle))
		setElementDimension(source, getElementDimension(vehicle))
	elseif sub == "gethere" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh przypisz [uid pojazdu]", source) end
		local vehicle = VehiclesManager.getVehicleByUid(vuid)

		if not vehicle then return exports.cstory_core:outputCommandError("Pojazd o tym uid nie jest zespawnowany", source) end

		local matrix = (source.matrix.position + source.matrix.forward * 3)
		local rx, ry, rz = getElementRotation(source)

		setElementPosition(vehicle, matrix.x, matrix.y, matrix.z)
		setElementInterior(vehicle, getElementInterior(source))
		setElementDimension(vehicle, getElementDimension(source))
	elseif sub == "kolor" then
		local vuid = tonumber(arg[2]) or 0
		if vuid <= 0 then return exports.cstory_core:outputCommandError("Tip: /(av)eh kolor [uid pojazdu]", source) end
		local vehicle = VehiclesManager.getVehicleByUid(vuid)

		if not vehicle then return exports.cstory_core:outputCommandError("Pojazd o tym uid nie jest zespawnowany", source) end

		triggerClientEvent(source, "showAvehKolor", resourceRoot, vehicle)
	else
		exports.cstory_core:outputCommandError("Tip: /(av)eh stworz/usun/spawn/unspawn/nearuid/zaparkuj/napraw/przypisz/goto/gethere/kolor", source)
	end
end
addCommandHandler("aveh", aVehCommand)
addCommandHandler("av", aVehCommand)


function avehPrzypiszResponse(ownertype, owner)
	if not getElementData(client, "character:char_id") or not getElementData(client, "character:adminDuty") or getElementData(client, "global:member_admin_level") < 2 then return end

	owner = owner or 0
	if owner == 0 then return end

	local vehicle = getElementData(client, "avehprzypisz_tmp")
	if not isElement(vehicle) then return end

	Vehicles[vehicle]['owner_type'] = ownertype
	Vehicles[vehicle]['owner'] = owner

	exports.cstory_db:query("UPDATE vehicles SET ownerType = ?, owner = ? WHERE id = ?", ownertype, owner, Vehicles[vehicle]['uid'])
	exports.cstory_core:outputInfoBox(string.format("Pojazd %s (UID: %d) został przypisany pod właściciela %s (UID: %d)", getVehicleName(vehicle), Vehicles[vehicle]['uid'], VehiclesManager.ownertypes[ownertype], owner), client)
end
addEvent("avehPrzypiszResponse", true)
addEventHandler("avehPrzypiszResponse", resourceRoot, avehPrzypiszResponse)


function avehKolorResponse(vehicle, r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4)
	if not getElementData(client, "character:char_id") or not getElementData(client, "character:adminDuty") or getElementData(client, "global:member_admin_level") < 2 then return end

	exports.cstory_db:query("UPDATE vehicles SET color = ? WHERE id = ?", toJSON({r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4}, true), Vehicles[vehicle]['uid'])
	exports.cstory_core:outputInfoBox(string.format("Kolor pojazdu %s (UID: %d) został zmieniony", getVehicleName(vehicle), Vehicles[vehicle]['uid']), client)
end
addEvent("avehKolorResponse", true)
addEventHandler("avehKolorResponse", resourceRoot, avehKolorResponse)