VehiclesManager = {}
Vehicles = {}

VehiclesManager.OWNER_TYPE_PLAYER = 1
VehiclesManager.OWNER_TYPE_GROUP = 2

VehiclesManager['ownertypes'] = {"Gracz", "Grupa"}

local actionType = {OPEN_BONNET = 1, OPEN_TRUNK = 2, LOCK_DOORS = 3, LIGHTS_ON = 4, ENGINE_ON = 5}

addEventHandler("onResourceStart", resourceRoot, function ()
	local amount, ntneed = VehiclesManager.load(string.format(" WHERE ownerType = %d", VehiclesManager.OWNER_TYPE_GROUP))

	outputDebugString(string.format("[cstory_vehicles] Wczytano %d pojazdów grupowych.", amount))	
end)

addEventHandler("onResourceStop", resourceRoot, function ()
	for k,v in ipairs(getElementsByType("vehicle")) do
		VehiclesManager.save(v)
	end
end)

-- [[ CSTORY_ACTIONS FALLBACK ]]--
addEventHandler("onResourceStart", root, function( res )
	if getResourceName(res) == "cstory_actions" then VehiclesManager.registerActions() end
end)

function VehiclesManager.registerActions()
	for k,v in pairs(Vehicles) do
		exports.cstory_actions:addAction(k, "vehicle", 4, false)
	end
end

function VehiclesManager.load(data)
	data = data or ""
	local query = string.format("SELECT * FROM vehicles%s",data)
	local vehicles, rows = exports.cstory_db:queryResult(query)


	if not rows then return 0 end
	if rows == 1 then vehicles = {vehicles} end

	local veh = nil
	for k,v in pairs(vehicles) do

		veh = createVehicle(v['model'], v['posX'], v['posY'], v['posZ'], v['rotX'], v['rotY'], v['rotZ'], v['numberplate'])

		Vehicles[veh] = {
			uid = v['id'],
			posx = v['posX'],
			posy = v['posY'],
			posz = v['posZ'],
			rotx = v['rotX'],
			roty = v['rotY'],
			rotz = v['rotZ'],
			dimension = v['dimension'],
			interior = v['interior'],
			owner_type = v['ownerType'],
			owner = v['owner'],
			last_saved = getTickCount(),
			locked = true,
			engine = false,
			lights = false
		}

		setElementHealth(veh, v['health'])
		setElementDimension(veh, v['dimension'])
		setElementInterior(veh, v['interior'])

		setVehicleColor(veh, unpack(fromJSON(v['color'])))
		setVehicleOverrideLights(veh, 1)
		setVehicleFuelTankExplodable(veh, false)
		setVehicleDamageProof(veh, true)
		setVehicleLocked(veh, true)

		setElementData(veh, 'vehicle:veh_id', v['id'])
		setElementData(veh, 'vehicle:locked', true)
		setElementData(veh, 'vehicle:mileage', v['mileage'])

		local visual_damage = fromJSON(v['visuals'])
		for i=0,6 do
			if i <= 5 then
				setVehicleDoorState(veh, i, visual_damage['doors'][tostring(i)])
			end

			setVehiclePanelState(veh, i, visual_damage['panels'][tostring(i)])
		end

		setVehicleWheelStates(veh, visual_damage['wheels'][1], visual_damage['wheels'][2], visual_damage['wheels'][3], visual_damage['wheels'][4])

		exports.cstory_actions:addAction(veh, "vehicle", 4, false)
	end
	return rows, veh
end

function VehiclesManager.unload(vehicle)
	VehiclesManager.save(vehicle)

	local vuid = getElementData(vehicle, "vehicle:veh_id")
	Vehicles[vehicle] = nil
	destroyElement(vehicle)
end

function VehiclesManager.save(vehicle)
	if getTickCount() - Vehicles[vehicle]['last_saved'] < 1000 then return end
	local visual_damage = {['doors'] = {}, ['panels'] = {}, ['wheels'] = {}}
	for i=0,6 do
		if i <= 5 then
			visual_damage['doors'][i] = getVehicleDoorState(vehicle, i)
		end

		visual_damage['panels'][i] = getVehiclePanelState(vehicle, i)
	end	

	visual_damage['wheels'][1], visual_damage['wheels'][2], visual_damage['wheels'][3], visual_damage['wheels'][4] = getVehicleWheelStates(vehicle)

	exports.cstory_db:query("UPDATE vehicles SET health = ?, visuals = ? WHERE id = ?", getElementHealth(vehicle), toJSON(visual_damage, true), Vehicles[vehicle]['uid'])

	Vehicles[vehicle]['last_saved'] = getTickCount()
	outputDebugString(string.format("[cstory_vehicles] Zapisano pojazd o UID: %d.", Vehicles[vehicle]['uid']))	
end

function VehiclesManager.delete(vuid)
	local vehicle = VehiclesManager.getVehicleByUid(vuid)

	if vehicle then
		VehiclesManager.unload(vehicle)
	end

	exports.cstory_db:query("DELETE FROM vehicles WHERE uid = ?", vuid)
end

function VehiclesManager.getVehicleByUid(vuid)
	for k,v in ipairs(getElementsByType("vehicle")) do
		if Vehicles[v]['uid'] == vuid then
			return v
		end
	end

	return false
end

function VehiclesManager.create(model, x, y, z, rx, ry, rz, dimension, interior)
	local row, rows, insertid = exports.cstory_db:queryResult(
		"INSERT INTO vehicles (model, posX, posY, posZ, rotX, rotY, rotZ, dimension, interior) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
		, tonumber(model), x, y, z, rx, ry, rz, tonumber(dimension), tonumber(interior))

	if insertid > 0 then
		VehiclesManager.load(string.format(" WHERE id = %d", insertid))
		return insertid
	end

	return false
end

function VehiclesManager.onPlayerAction(action_element, action_type)
	if getElementType(source) ~= "player" then return end
	if getElementType(action_element) ~= "vehicle" then return end

	if action_type == actionType.LOCK_DOORS then
		lockVehicleByPlayer(action_element, source)
	elseif action_type == actionType.LIGHTS_ON then
		vehicleAction(source, "lights_on", action_element)
	elseif action_type == actionType.ENGINE_ON then
		vehicleAction(source, "start_engine", action_element)
	elseif action_type == actionType.OPEN_TRUNK then
		vehicleAction(source, "open_trunk", action_element)
	elseif action_type == actionType.OPEN_BONNET then
		vehicleAction(source, "open_bonnet", action_element)
	end
end
addEventHandler("hook_onActionChoosen", root, VehiclesManager.onPlayerAction)

function onStartEnterVehicle(player, seat, jacked, door)
	local vehicle = source

	if Vehicles[vehicle]['locked'] then
		--cancelEvent()

		exports.cstory_core:outputInfoBox("Ten pojazd jest zamknięty", player)
	end
end
addEventHandler("onVehicleStartEnter", root, onStartEnterVehicle)

function onStartExitVehicle(player, seat, jacked, door)
	local vehicle = source
	setElementData(player, "is_truly_in_vehicle", false)
	if Vehicles[vehicle]['locked'] then
		cancelEvent()

		exports.cstory_core:outputInfoBox("Ten pojazd jest zamknięty", player)
	end
end
addEventHandler("onVehicleStartExit", root, onStartExitVehicle)

function onVehicleEnter(player, seat, jacked)
	local vehicle = source
	setElementData(player, "is_truly_in_vehicle", true)
	if seat == 0 then

		if not Vehicles[vehicle]['engine'] then setVehicleEngineState(vehicle, false) end
	end
end
addEventHandler("onVehicleEnter", root, onVehicleEnter)


function clientVehicleAction(type)
	if not client then return end	
	vehicleAction(client, type, getPedOccupiedVehicle(client))
end

function vehicleAction(player, type, vehicle)
	local seat = getPedOccupiedVehicleSeat(player)

	if not vehicle then return end

	if type == "start_engine" then
		if seat == false or seat > 0 then return end
		if not VehicleFunction.canPlayerUse(vehicle, player) then return exports.cstory_core:outputInfoBox("Nie posiadasz kluczy do tego pojazdu", player) end
		VehicleFunction.changeEngineState(vehicle)
	elseif type == "lights_on" then
		if seat == false or seat > 0 then return end
		VehicleFunction.changeLightsState(vehicle)
	elseif type == "open_trunk" then
		if getPedOccupiedVehicle(player) ~= vehicle and Vehicles[vehicle]['locked'] then
			return exports.cstory_core:outputInfoBox("Ten pojazd jest zamknięty", player)
		end

		local trunk = getVehicleDoorOpenRatio(vehicle, 1)

		if trunk == 0 then
			setVehicleDoorOpenRatio(vehicle, 1, 1, 3000)
		else
			setVehicleDoorOpenRatio(vehicle, 1, 0, 3000)
		end
	elseif type == "open_bonnet" then
		if getPedOccupiedVehicle(player) ~= vehicle and Vehicles[vehicle]['locked'] then
			return exports.cstory_core:outputInfoBox("Ten pojazd jest zamknięty", player)
		end

		local bonnet = getVehicleDoorOpenRatio(vehicle, 0)
		if bonnet == 0 then
			setVehicleDoorOpenRatio(vehicle, 0, 1, 3000)
		else
			setVehicleDoorOpenRatio(vehicle, 0, 0, 3000)
		end
	end
end
addEvent("onClientVehicleAction", true)
addEventHandler("onClientVehicleAction", resourceRoot, clientVehicleAction)

function updateVehicleMileage(vehicle, mileage)
	if not isElement(vehicle) then return end

	local current_mileage = getElementData(vehicle, "vehicle:mileage")

	-- byc moze hax, albo nie ruszyl z miejsca
	if mileage <= current_mileage then return end

	setElementData(vehicle, "vehicle:mileage", mileage)
	exports.cstory_db:query("UPDATE vehicles SET mileage = ? WHERE id = ?", mileage, Vehicles[vehicle]['uid'])
end
addEvent("updateVehicleMileage", true)
addEventHandler("updateVehicleMileage", resourceRoot, updateVehicleMileage)
