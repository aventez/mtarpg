addEvent("showAdminHelp", true)

Gui = {
    created = false,
    tabs = {},
    window,
    tabpanel,
    memos = {}
}

function adminHelpHandler()
    createGui()

    Gui.memos[1]:setText('WTF LOL')

    Gui.window:setVisible(true)
    showCursor(true)
end
addEventHandler("showAdminHelp", localPlayer, adminHelpHandler)

function createGui()
    if Gui.created then return end

    local screenW, screenH = guiGetScreenSize()

    Gui.window = guiCreateWindow(screenW/2 - (screenW*0.26093)/2, screenH/2 - (screenH*0.5641)/2, screenW*0.26093, screenH*0.5641, "Pomoc administratora", false)
    Gui.window:setSizable(false)

    Gui.tabpanel = guiCreateTabPanel(0.02, 0.03, 0.96, 0.95, true, Gui.window)

    Gui.tabs[1] = guiCreateTab("Główne", Gui.tabpanel)
    Gui.tabs[2] = guiCreateTab("Pojazdy", Gui.tabpanel)
    Gui.tabs[3] = guiCreateTab("Grupy", Gui.tabpanel)

    Gui.memos[1] = guiCreateMemo(0.02, 0.02, 0.96, 0.96, "", true, Gui.tabs[1])
    Gui.memos[1]:setProperty("Disabled", "True");

    Gui.memos[2] = guiCreateMemo(0.02, 0.02, 0.96, 0.96, "", true, Gui.tabs[2])
    Gui.memos[2]:setProperty("Disabled", "True");

    Gui.memos[3] = guiCreateMemo(0.02, 0.02, 0.96, 0.96, "", true, Gui.tabs[3])
    Gui.memos[3]:setProperty("Disabled", "True");

    -- zamykanie 
    local w = guiGetSize(Gui.window, false)
    local width = dxGetTextWidth("zamknij", 1, "default")
    local label = guiCreateLabel("right" == "left" and 5 or w - 5 - width, 2, width, 15, "zamknij", false, Gui.window)
    
    guiLabelSetColor(label, 160, 160, 160)
    guiLabelSetHorizontalAlign(label, "center", false)
    guiSetProperty(label, "ClippedByParent", "False")
    guiSetProperty(label, "AlwaysOnTop", "True")

    addEventHandler("onClientGUIClick", label, 
        function(button, state)
            if button == "left" and state == "up" then
                -- back
                Gui.window:setVisible(false)
                showCursor(false)
            end
        end, 
    false)

    addEventHandler("onClientMouseEnter", label, function() guiLabelSetColor(label, 255, 69, 59) end, false)
    addEventHandler("onClientMouseLeave", label, function() guiLabelSetColor(label, 160, 160, 160) end, false)
    --- zamykanie

    Gui.window:setVisible(false);
    Gui.created = true 
end