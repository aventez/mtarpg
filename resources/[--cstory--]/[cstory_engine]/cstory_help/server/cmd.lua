addCommandHandler("apomoc", function(source, commandName) 
    if not getElementData(source, "character:char_id") or not getElementData(source, "character:adminDuty") or getElementData(source, "global:member_admin_level") < 2 then return end

    triggerClientEvent(source, "showAdminHelp", source)
end)