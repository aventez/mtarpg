STREAM_DISTANCE = 15.0

local font = dxCreateFont("files/myriadproregular.ttf", 12, false, "default")


function renderNicknames()
	if not getElementData(localPlayer, "character:char_id") then return end

	local px, py, pz = getElementPosition(localPlayer)
	local x, y, z, size, alpha, text, distance

	for k, player in ipairs(getElementsByType("player")) do
		if isElementOnScreen(player) and localPlayer ~= player then
			x, y, z = getElementPosition(player)
			distance = getDistanceBetweenPoints3D(px, py, pz, x, y, z)
			if distance <= STREAM_DISTANCE then

				size = (STREAM_DISTANCE-distance)/STREAM_DISTANCE
				alpha = 200 * size
				if size < 0.7 then size = 0.7 end

				x, y = getScreenFromWorldPosition(x, y, z + 1.0)

				if x and y then

					text = string.format("%s (%d)", getElementData(player, "character:char_name"), getElementData(player, "character:id"))
					dxDrawText(text, x, y, x, y, tocolor(255, 255, 255, alpha), size, font, "center", "top", false, false, false, true, true)

					-- ---- STATUSY ------
					if getElementData(player, "character:chatInput") then
						dxDrawImage(x + 32*size, y - 32*size, 32*size, 32*size, "files/chat.png", 0, 0, 0, tocolor(255, 255, 255, 255), false) 
					end

					if getElementData(player, "character:afk") then
						dxDrawImage(x - 32*size, y - 32*size, 64*size, 32*size, "files/afk.png", 0, 0, 0, tocolor(255, 255, 255, 255), false) 
					end
				end
			end
		end
	end


	-- render naszego afk
	if getElementData(localPlayer, "character:afk") then
		x, y = guiGetScreenSize()
		dxDrawRectangle(0, 0, x, y, tocolor(0, 0, 0, 180), true)
		dxDrawImage(x/2-256, y/2-128, 512, 256, "files/afk2.png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
	end
end

addEventHandler("onClientRender", root, renderNicknames)