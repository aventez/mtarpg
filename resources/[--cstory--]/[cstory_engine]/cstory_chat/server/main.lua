-- podstawowy chat

function sendRadiusMessage(player, msg, radius, r, g, b, color_coded, not_to_myself)
	not_to_myself = not_to_myself or false
	r = r or 255
	g = g or 255
	b = b or 255
	color_coded = color_coded or false

	local players = getElementsByType("player")

	local x, y, z = getElementPosition(player)
	for _, v in ipairs(players) do
		if (not_to_myself and v ~= player) or not not_to_myself then
			if getElementData(v, "character:char_id") then
				x1, y1, z1 = getElementPosition(v)

				local distance = getDistanceBetweenPoints3D(x, y, z, x1, y1, z1)
				if distance <= radius then
					multip = 1 - ((distance / radius) * 0.5)
					outputChatBox(msg, v, r*multip, g*multip, b*multip, color_coded) 
				end
			end
		end
	end

end
function basicChat(message)
	if not getElementData(client, "character:char_id") then
		return false
	end

	if string.sub(message, 1, 1) == '/' then
		-- komenda
		local params = ""
		local cmd = ""
		local k = 0
		for i in string.gmatch(message, "%S+") do
		 	k = k + 1
		 	if k > 1 then
		 		params = params .. " " .. i
		 	else
		 		cmd = string.sub(i, 2)
		 	end
		end

		if commandExists(cmd) then
			executeCommandHandler(cmd, client, params)
		else
			exports.cstory_core:outputInfoBox('Taka komenda nie istnieje', client)
		end
	elseif string.sub(message, 1, 1) == '@' then
		local params = ""
		local slot = 0
		local k = 0
		for i in string.gmatch(message, "%S+") do
		 	k = k + 1
		 	if k > 1 then
		 		params = params .. " " .. i
		 	else
		 		slot = tonumber(string.sub(i, 2))
		 	end
		end

		-- tutaj funkcja ktora odpowiada za czat grupowy
		triggerEvent("onPlayerGroupChatSend", client, slot, params)
	else
		if message == ":P" or message == ":p" or message == ";p" or message == ";P" then
			executeCommandHandler("me", client, "wystawia język.")
		else
			sendRadiusMessage(client, string.format("%s mówi: %s", getElementData(client, "character:char_name"), message), 10)
		end
	end
end
addEvent("onPlayerChatRecent", true)
addEventHandler("onPlayerChatRecent", resourceRoot, basicChat)

addEvent("onPlayerGroupChatSend", false)


---- NOT EXISTING COMMANDS INFO ---
local command_handlers = {}

function updateCommandHandlersList(resource)
	command_handlers = getCommandHandlers()
end

addEventHandler("onResourceStart", getRootElement(), updateCommandHandlersList)
addEventHandler("onResourceStop", getRootElement(), updateCommandHandlersList)

function commandExists(command)
	local exist = false
	for _,subtable in pairs(command_handlers) do
		if subtable[1] == command then
			exist = true
			break
		end
	end

	return exist
end