function meCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") then return false end

	local arg = {...}
	
	local msg = table.concat(arg, " ")
	if string.len(msg) < 3 then return exports.cstory_core:outputCommandError("Tip: /me [akcja]", source) end

	sendRadiusMessage(source, string.format("* %s %s", getElementData(source, "character:char_name"), msg), 20, 185, 161, 255)
end
addCommandHandler("me", meCommand)


function doCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") then return false end

	local arg = {...}
	
	local msg = table.concat(arg, " ")
	if string.len(msg) < 3 then return exports.cstory_core:outputCommandError("Tip: /do [akcja]", source) end

	sendRadiusMessage(source, string.format("** %s (( %s ))", msg, getElementData(source, "character:char_name")), 20, 142, 153, 242)
end
addCommandHandler("do", doCommand)

function yellCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") then return false end

	local arg = {...}
	
	local msg = table.concat(arg, " ")
	if string.len(msg) < 3 then return exports.cstory_core:outputCommandError("Tip: /k [tekst]", source) end

	sendRadiusMessage(source, string.format("%s krzyczy: %s", getElementData(source, "character:char_name"), msg), 20, 255, 255, 255)
end
addCommandHandler("k", yellCommand)

function silentCommand(source, cmd, targetid, ...)
	if not getElementData(source, "character:char_id") then return false end

	targetid = tonumber(targetid)
	if not targetid or targetid < 1 then return exports.cstory_core:outputCommandError("Tip: /s [id gracza] [teskt]", source) end
	local target = exports.cstory_id:getPlayerByID(targetid)
	if not target then return exports.cstory_core:outputCommandError("Gracz o podanym id nie istnieje.", source) end
	if target == source then return exports.cstory_core:outputCommandError("Nie możesz szeptać do siebie.", source) end

	local arg = {...}
	local msg = table.concat(arg, " ")
	if string.len(msg) < 3 then return exports.cstory_core:outputCommandError("Tip: /s [id gracza] [tekst]", source) end

	outputChatBox(string.format("%s szepcze: %s", getElementData(source, "character:char_name"), msg), target, 100, 100, 100)
	outputChatBox(string.format("%s szepcze: %s", getElementData(source, "character:char_name"), msg), source, 255, 255, 255)
	executeCommandHandler("me", source, string.format("szepcze coś do ucha %s.", getElementData(target, "character:char_name")))
end
addCommandHandler("s", silentCommand)

function oocCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") then return false end

	local arg = {...}
	
	local msg = table.concat(arg, " ")
	if string.len(msg) < 3 then return exports.cstory_core:outputCommandError("Tip: /b [tekst ooc]", source) end

	sendRadiusMessage(source, string.format("(( [%d] %s: %s ))", tonumber(getElementData(source, "character:id")), getElementData(source, "character:char_name"), msg), 20, 180, 180, 180)
end
addCommandHandler("b", oocCommand)

function whisperCommand(source, cmd, targetid, ...)
	if not getElementData(source, "character:char_id") then return false end

	targetid = tonumber(targetid)
	if not targetid or targetid < 1 then return exports.cstory_core:outputCommandError("Tip: /w [id gracza] [teskt]", source) end
	local target = exports.cstory_id:getPlayerByID(targetid)
	if not target then return exports.cstory_core:outputCommandError("Gracz o podanym id nie istnieje.", source) end
	if target == source then return exports.cstory_core:outputCommandError("Nie możesz pisać do siebie.", source) end

	local arg = {...}
	local msg = table.concat(arg, " ")
	if string.len(msg) < 3 then return exports.cstory_core:outputCommandError("Tip: /w [id gracza] [tekst]", source) end

	outputChatBox(string.format("(( %s (%d): %s ))", getElementData(source, "character:char_name"), tonumber(getElementData(source, "character:id")), msg), target, 255, 196, 71)
	outputChatBox(string.format("(( > %s (%d): %s ))", getElementData(target, "character:char_name"), tonumber(getElementData(target, "character:id")), msg), source, 252, 211, 132)
end
addCommandHandler("w", whisperCommand)