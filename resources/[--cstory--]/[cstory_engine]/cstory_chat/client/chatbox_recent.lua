local ChatRecent = {
	lines = {},
	cur_line = 0,
	inputBox = nil,
	visible = false
}

function playerPressedKey(button, press)
	if ChatRecent.visible then
		if not (isConsoleActive() or isMainMenuActive()) then
			guiBringToFront(ChatRecent.inputBox)
		end
	end
    if press then
    	if ChatRecent.visible then
	    	if button == "arrow_u" then
	    		ChatRecent.cur_line = ChatRecent.cur_line + 1
	    		if ChatRecent.lines[ChatRecent.cur_line] == nil then ChatRecent.cur_line = ChatRecent.cur_line - 1 end
	    		if ChatRecent.cur_line > 20 then ChatRecent.cur_line = 20 end

	    		if ChatRecent.cur_line > 0 then
		    		guiSetText(ChatRecent.inputBox, ChatRecent.lines[ChatRecent.cur_line])
		    		guiEditSetCaretIndex(ChatRecent.inputBox, string.len(ChatRecent.lines[ChatRecent.cur_line]))
	    		end
	    	elseif button == "arrow_d" then
	    		ChatRecent.cur_line = ChatRecent.cur_line - 1
	    		if ChatRecent.cur_line < 0 then ChatRecent.cur_line = 0 end

	    		if ChatRecent.cur_line > 0 then 
	    			guiSetText(ChatRecent.inputBox, ChatRecent.lines[ChatRecent.cur_line])
	    			guiEditSetCaretIndex(ChatRecent.inputBox, string.len(ChatRecent.lines[ChatRecent.cur_line]))
    			else 
    				guiSetText(ChatRecent.inputBox, "") 
    				guiEditSetCaretIndex(ChatRecent.inputBox, 0)
    			end
	    	end
    	end
    	if button == "enter" then
    		if ChatRecent.visible then
    			local msg = guiGetText(ChatRecent.inputBox)
    			-- wysylamy wiadomosc
    			if string.len(msg) > 0 then
    				if string.sub(msg, 1, 1) == '/' then
						-- komenda
						local params = ""
						local cmd = ""
						local k = 0
						for i in string.gmatch(msg, "%S+") do
						 	k = k + 1
						 	if k > 1 then
						 		params = params .. " " .. i
						 	else
						 		cmd = string.sub(i, 2)
						 	end
						end

						local result = executeCommandHandler(cmd, params)
						if not result then triggerServerEvent("onPlayerChatRecent", resourceRoot, msg) end
					else
						triggerServerEvent("onPlayerChatRecent", resourceRoot, msg)
					end
    				

    				local tmp
	        		local tmp2 = ChatRecent.lines[1]
	        		for i = 2, 20, 1 do
	        			tmp = ChatRecent.lines[i]
	        			ChatRecent.lines[i] = tmp2
	        			tmp2 = tmp
	        		end

	        		ChatRecent.lines[1] = msg
    			end
    			-- chowamy
	    		guiSetVisible(ChatRecent.inputBox, false)
	    		showCursor(false)
	    		guiSetInputEnabled(false)
	    		ChatRecent.visible = false
	    		ChatRecent.cur_line = 0
	    		setElementData(localPlayer, "character:chatInput", false)

    		end

    		
    	elseif button == "t" then
    		if not ChatRecent.visible and not guiGetInputEnabled() then

        		guiSetVisible(ChatRecent.inputBox, true)
        		showCursor(true)
        		guiSetInputEnabled(true)
        		guiSetText(ChatRecent.inputBox, "")
        		ChatRecent.visible = true
        		setCursorPosition(80, 210)
        		setElementData(localPlayer, "character:chatInput", true)
        		setTimer(function ()
        			guiBringToFront(ChatRecent.inputBox) 
        			guiEditSetCaretIndex(ChatRecent.inputBox, 0)
        		end, 50, 1)
    		end
    	end
    end
end
addEventHandler("onClientKey", root, playerPressedKey)

addEventHandler("onClientResourceStart", resourceRoot,
    function()
    	setTimer(function()
    		toggleControl("chatbox", false)
    	end, 2000, 0)
    	ChatRecent.inputBox = guiCreateEdit(24, 180, 542, 25, "", false)
    	guiEditSetMaxLength(ChatRecent.inputBox, 160)
    	guiSetVisible(ChatRecent.inputBox, false)
   	end
)