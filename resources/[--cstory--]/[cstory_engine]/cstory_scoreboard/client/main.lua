----------------------------------------------------
-- Skrypt Multi Theft Auto RolePlay
-- Autor: Hifi
----------------------------------------------------

local scoreFunc = {}
local sW, sH = guiGetScreenSize()
scoreFunc.font = dxCreateFont("client/files/MyriadPro-Regular.otf", 10, false)
scoreFunc.font16 = dxCreateFont("client/files/MyriadPro-Regular.otf", 16, false)
scoreFunc.blur = dxCreateShader("client/files/BlurShader.fx")
scoreFunc.screen_source = dxCreateScreenSource(sW, sH)
scoreFunc.page = 1

scoreFunc.alpha = 0.0

scoreFunc.onPage = 18
scoreFunc.renderData = {sW / 2 - 281, sH / 2 - 222, 512, 105}
scoreFunc.renderState = 0
scoreFunc.renderTime = 0
scoreFunc.interpolateTime = 250

function scoreFunc.sort(op1, op2)
	if isElement(op1) and isElement(op2) then
		return op1:getData("character:id") < op2:getData("character:id")
	end
end

function scoreFunc.render()
	if scoreFunc.renderState == 1 then
		local progress = (getTickCount() - scoreFunc.renderTime) / scoreFunc.interpolateTime
		scoreFunc.alpha = interpolateBetween(0, 0, 0, 1, 0, 0, progress, "Linear")*255

		if progress > 1 then
			scoreFunc.renderState = 2
			addEventHandler("onClientKey", root, scoreFunc.scrollList)
		end
	elseif scoreFunc.renderState == 3 then
		local progress = (getTickCount() - scoreFunc.renderTime) / scoreFunc.interpolateTime
		scoreFunc.alpha = interpolateBetween(1, 0, 0, 0, 0, 0, progress, "Linear")*255

		if progress > 1 then
			scoreFunc.renderState = 0
			removeEventHandler("onClientRender", root, scoreFunc.render)
			removeEventHandler("onClientKey", root, scoreFunc.scrollList)
	    end
	end
	
	local allPlayers = {}
	allPlayers = scoreFunc.getPlayers(true)

	table.sort(allPlayers, scoreFunc.sort)
	local _allPlayers = allPlayers
	allPlayers = {}
	table.insert(allPlayers, localPlayer)
	for i = 1, #_allPlayers do
		allPlayers[i + 1] = _allPlayers[i]
	end
	_allPlayers = nil

	dxUpdateScreenSource(scoreFunc.screen_source)
    
    dxSetShaderValue(scoreFunc.blur, "ScreenSource", scoreFunc.screen_source);
    dxSetShaderValue(scoreFunc.blur, "BlurStrength", 5);
	dxSetShaderValue(scoreFunc.blur, "UVSize", sW, sH);

	if( scoreFunc.renderState == 2 or scoreFunc.renderState == 1 ) then dxDrawImageSection(scoreFunc.renderData[1] + 30, scoreFunc.renderData[2] + 20, scoreFunc.renderData[3] - 64, scoreFunc.renderData[4] - 2 + (#allPlayers * 18), scoreFunc.renderData[1] + 30, scoreFunc.renderData[2] + 20, scoreFunc.renderData[3] - 64, scoreFunc.renderData[4] + 30 + (#allPlayers * 18), scoreFunc.blur, 0, 0, 0, tocolor(255, 255, 255, scoreFunc.alpha)) end
	dxDrawImage(scoreFunc.renderData[1], scoreFunc.renderData[2], scoreFunc.renderData[3], scoreFunc.renderData[4], "client/files/top.png", 0, 0, 0, tocolor(255, 255, 255, scoreFunc.alpha))


	local i = 1
	for k, v in ipairs(allPlayers) do
		local row = "row1"
		if i%2 == 0 then row = "row2" end
		dxDrawImage(scoreFunc.renderData[1], scoreFunc.renderData[2] + 105 + (i - 1) * 18, 512, 18, string.format("client/files/%s.png", row), 0, 0, 0, tocolor(255, 255, 255, scoreFunc.alpha))
		dxDrawText(v:getData("character:id") or "??", scoreFunc.renderData[1] + 33, scoreFunc.renderData[2] + 105 + (i - 1) * 18, scoreFunc.renderData[1] + 67, scoreFunc.renderData[2] + 123 + (i - 1) * 18, tocolor(255, 255, 255, scoreFunc.alpha), 1.0, scoreFunc.font, "center", "center")
		local playerName = getElementData(v, "character:char_name")
		if getElementData(v, "character:adminDuty") then
			playerName = getElementData(v, "global:member_name");
		end
		playerName = string.gsub(playerName, "#%x%x%x%x%x%x", "")
		dxDrawText(playerName, scoreFunc.renderData[1] + 76, scoreFunc.renderData[2] + 105 + (i - 1) * 18, scoreFunc.renderData[1] + 300, scoreFunc.renderData[2] + 123 + (i - 1) * 18, tocolor(255, 255, 255, scoreFunc.alpha), 1.0, scoreFunc.font, "left", "center")

		dxDrawText(v:getData("global:member_gamescore") or "0", scoreFunc.renderData[1] + 371, scoreFunc.renderData[2] + 105 + (i - 1) * 18, scoreFunc.renderData[1] + 371, scoreFunc.renderData[2] + 123 + (i - 1) * 18, tocolor(255, 255, 255, scoreFunc.alpha), 1.0, scoreFunc.font, "center", "center")

		dxDrawText(getPlayerPing(v), scoreFunc.renderData[1] + 455, scoreFunc.renderData[2] + 105 + (i - 1) * 18, scoreFunc.renderData[1] + 455, scoreFunc.renderData[2] + 123 + (i - 1) * 18, tocolor(255, 255, 255, scoreFunc.alpha), 1.0, scoreFunc.font, "center", "center")

		--dxDrawText(v:getData("character:char_id") or "?????", scoreFunc.renderData[1] + 105, scoreFunc.renderData[2] + 43 + (i - 1) * 20, scoreFunc.renderData[1] + 186, scoreFunc.renderData[2] + 63 + (i - 1) * 20, tocolor(215, 215, 215, scoreFunc.alpha), 1.0, scoreFunc.font, "center", "center")
		
		--local playerName = getElementData(v, "character:char_name")
		--playerName = string.gsub(playerName, "#%x%x%x%x%x%x", "")
		--dxDrawText(playerName, scoreFunc.renderData[1] + 195, scoreFunc.renderData[2] + 43 + (i - 1) * 20, scoreFunc.renderData[1] + 335, scoreFunc.renderData[2] + 63 + (i - 1) * 20, tocolor(215, 215, 215, scoreFunc.alpha), 1.0, scoreFunc.font, "center", "center")
		
		i = i + 1
	end

	dxDrawImage(scoreFunc.renderData[1], scoreFunc.renderData[2] + 105 + (i - 1) * 18, scoreFunc.renderData[3], 50, "client/files/bottom.png", 0, 0, 0, tocolor(255, 255, 255, scoreFunc.alpha))
	dxDrawText(string.format("%d/%d", #allPlayers, 100), scoreFunc.renderData[1] + 420, scoreFunc.renderData[2] + 56, scoreFunc.renderData[1] + 420, scoreFunc.renderData[2] + 56, tocolor(183, 50, 65, scoreFunc.alpha), 1, scoreFunc.font16, "center", "center")
end

function scoreFunc.bindKey(key, state)
	if localPlayer:getData("character:char_id") then
		if state == "down" and scoreFunc.renderState == 0 then
			addEventHandler("onClientRender", root, scoreFunc.render)
			scoreFunc.renderState = 1
			scoreFunc.renderTime = getTickCount()
		elseif scoreFunc.renderState == 2 then
			scoreFunc.renderState = 3
			scoreFunc.renderTime = getTickCount()
		end	
	end
end
bindKey("tab", "both", scoreFunc.bindKey)

function scoreFunc.scrollList(button, press)
	if press then
		if button == "mouse_wheel_up" then
			scoreFunc.page = scoreFunc.page - 1
			if scoreFunc.page < 1 then scoreFunc.page = 1 end
		end
		if button == "mouse_wheel_down" then
			local players = #scoreFunc.getPlayers(false)
			if players >= (scoreFunc.page) * scoreFunc.onPage then
				scoreFunc.page = scoreFunc.page + 1
			end
		end
	end
end

function scoreFunc.getPlayers(without_local)
	local players = {}
	for k, v in ipairs(getElementsByType("player")) do
		if (v:getName() == localPlayer:getName() and not without_local) or v:getName() ~= localPlayer:getName() then
			if getElementData(v, "character:char_id") then
				table.insert(players, v)
			end
		end
	end
	return players
end