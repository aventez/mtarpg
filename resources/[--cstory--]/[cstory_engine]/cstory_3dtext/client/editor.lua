local Editor3dText = {}

Editor3dText.selectionMode = false
Editor3dText.selectionHoverId = 0
Editor3dText.selectionCursorPosition = {0, 0}

function enable3dTextSelection()
	if Editor3dText.selectionMode then return false end

	Editor3dText.selectionMode = true
	showCursor(true, false)
	toggleAllControls(false)
	addEventHandler("onClientKey", root, selectionKeyPress)
end

function disable3dTextSelection()
	Editor3dText.selectionMode = false
	Editor3dText.selectionHoverId = 0
	showCursor(false, true)
	toggleAllControls(true)

	removeEventHandler("onClientKey", root, selectionKeyPress)
end

function selectionKeyPress(key, press)
	if key == "space" and press then
		
		local x, y = getCursorPosition()
		local sw, sh = guiGetScreenSize()
		Editor3dText.selectionCursorPosition = { x * sw, y * sh }

		showCursor(false, false)
	elseif key == "space" and not press then
		showCursor(true, false)
		setCursorPosition(Editor3dText.selectionCursorPosition[1], Editor3dText.selectionCursorPosition[2])
	end

	if key == "mouse1" and press and Editor3dText.selectionHoverId > 0 then
		local id = Editor3dText.selectionHoverId

		disable3dTextSelection()
		enable3dTextEdit(id)
	end

	if key == "escape" then
		disable3dTextSelection()
		cancelEvent()
	end
end

addEvent("enable3dTextEditorSelection", true)
addEventHandler("enable3dTextEditorSelection", root, enable3dTextSelection)

addEventHandler( "onClientCursorMove", root,
    function ( _, _, sx, sy )
    	if not Editor3dText.selectionMode then return end
    	if not isCursorShowing() then return end
        -- tutaj szukamy 3dtextow
        local px, py, pz = getElementPosition(localPlayer)

        local texts = Buffer3dText.getVisibleTexts()
        local pretender_id, pretender_distance = 0, 40

        if texts then
	        for k, v in ipairs(texts) do
	        	local distance = getDistanceBetweenPoints2D(sx, sy, v.screen_position.x, v.screen_position.y)
	        	if distance <= pretender_distance then
	        		pretender_distance = distance
	        		pretender_id = k
	        	end
	        end
    	end

        if pretender_id > 0 then
        	if getDistanceBetweenPoints3D(px, py, pz, texts[pretender_id].position[1], texts[pretender_id].position[2], texts[pretender_id].position[3]) <= 20 then
        		Editor3dText.selectionHoverId = pretender_id
        	else
        		Editor3dText.selectionHoverId = 0

        	end
        else
        	Editor3dText.selectionHoverId = 0
    	end
    end
)

addEventHandler("onClientRender", root,
	function ()
		if Editor3dText.selectionMode and Editor3dText.selectionHoverId > 0 then
			local text = Buffer3dText.getTextByCid(Editor3dText.selectionHoverId)

			local sw, sh = guiGetScreenSize()
			dxDrawRectangle(sw * 0.4, sh * 0.75, sw * 0.2, sh * 0.11, tocolor(0, 0, 0, 180))
			dxDrawText(string.format("#D4A190%s...\n#FFFFFFclientid: %d, serverid: %d \n\nAby zakończyć wybieranie wciśnij ESC.", string.sub(string.gsub(text.text, "\n", "\\n"), 1, 30), Editor3dText.selectionHoverId, text.serverid), sw * 0.4 + 10, (sh * 0.75) + 10, sw * 0.6 - 20, sh * 0.86 + 20, tocolor(255, 255, 255), 1, "default", "left", "top", false, false, false, true)
		end
	end
)
