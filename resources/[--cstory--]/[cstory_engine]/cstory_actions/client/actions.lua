local distance_fallback_timer
local actionOn = false

addEvent("onActionFired", true)
addEvent("onActionEnd", true)

function keyListener(button, press)
	if not getElementData(localPlayer, "character:char_id") then return end

	if button == "e" and press and not getElementData(localPlayer, "character:chatInput") then
		local cancel_event = onActionKeyPress()
		if cancel_event then cancelEvent() end
		
		if not distance_fallback_timer then 
			distance_fallback_timer = setTimer(distanceFallbackCheck, 500, 0) 
		end
	elseif button == "e" and not press and not getElementData(localPlayer, "character:chatInput") then
		onActionKeyRelease()
	end
end
addEventHandler("onClientKey", root, keyListener)

function onActionKeyRelease()
	if actionOn then
		triggerServerEvent("onClientReleaseActionButton", resourceRoot)
		actionOn = false
	end
end

function onActionKeyPress()
	if actionOn then return end

	local pretender_distance = 20.0
	local pretender_element = nil
	local pretender_vehicle_distance = 5.0
	local pretender_vehicle = nil
	local distance
	local x, y, z = getElementPosition(localPlayer)
	local ax, ay, az
	local return_val = false

	if getPedOccupiedVehicle(localPlayer) and getPedOccupiedVehicleSeat(localPlayer) == 0 then
		pretender_element = getPedOccupiedVehicle(localPlayer)
		return_val = true
	else

		for j, k in ipairs(getElementsByType("object")) do
			if getElementData(k, "action:type") then
				if (getElementDimension(k) == getElementDimension(localPlayer)) and (getElementInterior(k) == getElementInterior(localPlayer)) then
					ax, ay, az = getElementPosition(k)
					distance = getDistanceBetweenPoints3D(x, y, z, ax, ay, az)
					if distance < pretender_distance and distance <= getElementData(k, "action:radius")  then
						pretender_distance = distance
						pretender_element = k
					end
				end
			end
		end

		

		for j, k in ipairs(getElementsByType("vehicle")) do
			if (getElementDimension(k) == getElementDimension(localPlayer)) and (getElementInterior(k) == getElementInterior(localPlayer)) then
				ax, ay, az = getElementPosition(k)
				distance = getDistanceBetweenPoints3D(x, y, z, ax, ay, az)
				if distance < pretender_vehicle_distance and distance <= getElementData(k, "action:radius") then
					pretender_vehicle_distance = distance
					pretender_vehicle = k
				end
			end
		end
	end

	if pretender_element and pretender_vehicle then
		if pretender_vehicle_distance < pretender_distance then
			pretender_element = pretender_vehicle
		end
	end

	if not pretender_element and pretender_vehicle then
		pretender_element = pretender_vehicle
	end

	if pretender_element then
		-- znalezlismy akcje w poblizu
		actionOn = true
		triggerServerEvent("onClientFoundAction", resourceRoot, pretender_element)
	end

	return return_val
end

function distanceFallbackCheck()
	if getElementData(localPlayer, "inn_action_pos") then
		local pos = getElementData(localPlayer, "inn_action_pos")
		local x, y, z = getElementPosition(localPlayer)

		if getDistanceBetweenPoints3D(x, y, z, pos[1], pos[2], pos[3]) > pos[4] then
			triggerServerEvent("onClientReleaseActionButton", resourceRoot)
		end
		last_checked_distance = getTickCount()
	else 
		killTimer(distance_fallback_timer)
		distance_fallback_timer = nil
	end
end