local screenW, screenH = guiGetScreenSize()
local font = dxCreateFont("files/myriadproregular.ttf", 14, false, "cleartype")
local anim = {
	circles = {}
}
local element = nil
local elements = {}
local selected = nil

local actionType = {OPEN_BONNET = 1, OPEN_TRUNK = 2, LOCK_DOORS = 3, LIGHTS_ON = 4, ENGINE_ON = 5}
local actionData = {
	[actionType.OPEN_BONNET] = {['image'] = "files/icons/maska.png", ['title'] = "Podnieś maskę"},
	[actionType.OPEN_TRUNK] = {['image'] = "files/icons/bagaznik.png", ['title'] = "Otwórz bagażnik"},
	[actionType.LOCK_DOORS] = {['image'] = "files/icons/zamknijotworz.png", ['title'] = "Otwórz pojazd"},
	[actionType.LIGHTS_ON] = {['image'] = "files/icons/swiatla.png", ['title'] = "Włącz światła"},
	[actionType.ENGINE_ON] = {['image'] = "files/icons/silnikstart.png", ['title'] = "Uruchom silnik"}
}


function getActionTitle(type, data1)

	if data1 and getElementType(data1) == "vehicle" then
		if type == actionType.OPEN_BONNET then
			if getVehicleDoorOpenRatio(data1, 0) == 0 then return "Otwórz maskę"
			else return "Zamknij maskę" end
		elseif type == actionType.OPEN_TRUNK then
			if getVehicleDoorOpenRatio(data1, 1) == 0 then return "Otwórz bagażnik"
			else return "Zamknij bagażnik" end
		elseif type == actionType.ENGINE_ON then
			if getVehicleEngineState(data1) then return "Zgaś silnik"
			else return "Odpal silnik" end
		elseif type == actionType.LIGHTS_ON then
			if getVehicleOverrideLights(data1) == 1 then return "Włącz światła"
			else return "Wyłącz światła" end
		elseif type == actionType.LOCK_DOORS then
			if getElementData(data1, 'vehicle:locked') then return "Otwórz pojazd"
			else return "Zamknij pojazd" end
		end
	end
end

function onCircleInterfaceAction(action_element, actiontype, action_elements)
	if not isElement(action_element) then return end
	if getElementData(action_element, "action:custom_interact") then return end
	if not action_elements then
		onActionKeyRelease()
		return
	end
	element = action_element
	elements = action_elements
	showCursor(true)
	setCursorAlpha(0)
	setCursorPosition(screenW/2 - 150, screenH/2 - 150)

	anim.circles = {[1] = {['rotation'] = 0}, [2] = {['rotation'] = 40}}

	addEventHandler("onClientRender", root, renderTest)
	addEventHandler("onClientKey", root, onMouseClick)
end
addEventHandler("onActionFired", localPlayer, onCircleInterfaceAction)

function onCircleInterfaceActionEnd(actiontype, custom_interact)
	if custom_interact then return end
	
	removeEventHandler("onClientRender", root, renderTest)
	removeEventHandler("onClientKey", root, onMouseClick)
	showCursor(false)
	setCursorAlpha(255)
end
addEventHandler("onActionEnd", localPlayer, onCircleInterfaceActionEnd)

function renderTest()
	local x,y = screenW/2, screenH/2
	local absoluteX, absoluteY = getCursorPosition()
	absoluteX = absoluteX * screenW
	absoluteY = absoluteY * screenH

	local out_of = false
	-- bigger outiside
	if absoluteX < x - 200 then absoluteX = x-200 out_of = true end
	if absoluteX > x + 200 then absoluteX = x+200 out_of = true end

	-- bigger outside
	if absoluteY < y - 200 then absoluteY = y-200 out_of = true end
	if absoluteY > y + 200 then absoluteY = y+200 out_of = true end

	if out_of then setCursorPosition(absoluteX, absoluteY) end

	local length = math.sqrt(math.pow(absoluteX - x, 2) + math.pow((screenH - absoluteY) - (screenH - (screenH/2)), 2))

	-- liczymy kat
	local deltaY = (screenH - absoluteY) - y
	local deltaX = absoluteX - (x - length)

	local degree = math.atan2(deltaX, deltaY) *360 / math.pi

	local single_option_degree = 360 / #elements

	local angles = {0, 0, 30, 45, 53, 60}

	for i=1,#elements do
		local is_selected = ""

		local start_degree = angles[#elements] + (i-1)*single_option_degree
		local stop_degree = start_degree + single_option_degree
		if stop_degree > 360 then 
			if (degree >= start_degree and degree <= stop_degree) or (degree >= 0 and degree <= (stop_degree-360)) then is_selected = "Selected" end
		else
			if degree >= start_degree and degree <= stop_degree then is_selected = "Selected" end
		end

		if is_selected == "Selected" then selected = i end

		local imgX = math.cos( math.rad( (i-1)*single_option_degree ) - math.rad(90) ) * 140 + (x-64)
		local imgY = math.sin( math.rad( (i-1)*single_option_degree ) - math.rad(90) ) * 140 + (y-32)

		dxDrawImage(x-256, y-256, 512, 512, string.format("files/outside%s%d.png", is_selected, #elements), (i-1)*single_option_degree, 0, 0, tocolor(255, 255, 255, 255), false)
		dxDrawImage(imgX, imgY, 128, 64, actionData[elements[i]]['image'], (i-1)*single_option_degree, 0, 0, tocolor(255, 255, 255, 255), false)
	end

	local arrow_degree = degree - 90
	if arrow_degree < 0 then arrow_degree = 360 + arrow_degree end

	dxDrawImage(x-128, y-128, 256, 256, "files/inside.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
	dxDrawImage(x-128, y-128, 256, 256, "files/insidecircle1.png", anim.circles[1]['rotation'], 0, 0, tocolor(255, 255, 255, 255), false)
	dxDrawImage(x-128, y-128, 256, 256, "files/insidecircle2.png", anim.circles[2]['rotation'], 0, 0, tocolor(255, 255, 255, 255), false)
	dxDrawImage(x-128, y-128, 256, 256, "files/arrow.png", arrow_degree, 0, 0, tocolor(255, 255, 255, 255), false)
	dxDrawText(getActionTitle(elements[selected], element), x, y, x, y, tocolor(255, 255, 255, 255), 0.8, font, "center", "center", false, false, false, false, true)

	-- animation
	anim.circles[1]['rotation'] = anim.circles[1]['rotation'] + 0.5
	anim.circles[2]['rotation'] = anim.circles[2]['rotation'] + 0.2

	if(anim.circles[1]['rotation'] >= 360) then anim.circles[1]['rotation'] = 0 end
	if(anim.circles[2]['rotation'] >= 360) then anim.circles[2]['rotation'] = 0 end
end

function onMouseClick(button, press)
	if button == "mouse1" and press then
		--- wysylamy element na ktorym jest przeprowadzana akcja oraz typ wybranej akcji
		triggerServerEvent("onClientChooseAction", resourceRoot, element, elements[selected])
		onActionKeyRelease()
	end
end