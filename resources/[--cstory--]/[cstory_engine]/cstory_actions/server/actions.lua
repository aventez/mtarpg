local actions = {}
local actionOn = {}

local actionType = {OPEN_BONNET = 1, OPEN_TRUNK = 2, LOCK_DOORS = 3, LIGHTS_ON = 4, ENGINE_ON = 5}

function addAction(element, type, radius, custom_interact)
	if not element then return end
	if getElementType(element) ~= "object" and getElementType(element) ~= "vehicle" then return end
	radius = radius or 5.0

	setElementData(element, "action:type", type)
	setElementData(element, "action:radius", radius)
	if custom_interact then
		setElementData(element, "action:custom_interact", true)
	end
end

function removeAction(element)
	removeElementData(element, "action:type")
	removeElementData(element, "action:radius")
	removeElementData(element, "action:custom_interact")
end

function onActionFound(action_element)
	if action_element then
		actionOn[client] = action_element
		-- fallback gdyby odszedl za daleko (zrobione po stronie clienta)
		local x, y, z = getElementPosition(action_element)
		setElementData(client, "inn_action_pos", {x, y, z, getElementData(action_element, "action:radius")})

		-- sprwadzamy jakie akcje moze wykonac na danym elemencie
		local actions = nil
		if not getElementData(action_element, "action:custom_interact") then
			if getElementType(action_element) == "vehicle" and (getPedOccupiedVehicle(client) ~= action_element or (getPedOccupiedVehicle(client) == action_element and not getElementData(client, "is_truly_in_vehicle"))) then
				-- ELEMENT: POJAZD; GRACZ POZA POJAZDEM
				actions = {actionType.LOCK_DOORS, actionType.OPEN_BONNET, actionType.OPEN_TRUNK}
			elseif getElementType(action_element) == "vehicle" and getPedOccupiedVehicle(client) == action_element and getPedOccupiedVehicleSeat(client) == 0 and getElementData(client, "is_truly_in_vehicle") then
				-- ELEMENT: POJAZD; GRACZ W POJEŹDZIE
				actions = {actionType.ENGINE_ON, actionType.LIGHTS_ON, actionType.LOCK_DOORS, actionType.OPEN_BONNET, actionType.OPEN_TRUNK}
			end
		end
		triggerClientEvent(client, "onActionFired", client, action_element, getElementData(action_element, "action:type"), actions)
	end
end

addEvent("onClientFoundAction", true)
addEventHandler("onClientFoundAction", resourceRoot, onActionFound)

function onActionKeyRelease()
	if not actionOn[client] then return end
	removeElementData(client, "inn_action_pos")
	triggerClientEvent(client, "onActionEnd", client, getElementData(actionOn[client], "action:type"), getElementData(actionOn[client], "action:custom_interact"))
	actionOn[client] = nil
end



addEvent("onClientReleaseActionButton", true)
addEventHandler("onClientReleaseActionButton", resourceRoot, onActionKeyRelease)


function onActionChoosen(action_element, action_type)
	triggerEvent("hook_onActionChoosen", client, action_element, action_type)
end

addEvent("hook_onActionChoosen")

addEvent("onClientChooseAction", true)
addEventHandler("onClientChooseAction", resourceRoot, onActionChoosen)