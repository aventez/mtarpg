local dbHandler = 
{
	queries = 0,
	connectDate = 0,

	config = {["host"] = "127.0.0.1", ["user"] = "csp", ["pass"] = "pass", ["db"] = "mtadm"},

	dbConnection = nil
}

function dbHandler.connect()
	dbHandler.dbConnection = dbConnect("mysql", string.format("dbname=%s;host=%s", dbHandler.config.db, dbHandler.config.host), dbHandler.config.user, dbHandler.config.pass, "share=1")
	if dbHandler.dbConnection then
		outputDebugString("[cstory_db] Połączono z bazą danych.")
		dbHandler.connectDate = getRealTime().timestamp
	else
		outputDebugString("[cstory_db] Błąd łączenia z bazą danych.")
	end
end
addEventHandler("onResourceStart", resourceRoot, dbHandler.connect)

function dbHandler.disconnect()
	if isElement(dbHandler.dbConnection) then
		destroyElement(dbHandler.dbConnection)
	end
	outputDebugString(string.format("[cstory_db] Rozłączono z bazą danych. | Wykonano %d zapytań. | Połączenie trwało %d minut.", dbHandler.queries, math.floor((getRealTime().timestamp - dbHandler.connectDate) / 60)))
end
addEventHandler("onResourceStop", resourceRoot, dbHandler.disconnect)

function queryResult(...)
	dbHandler.queries = dbHandler.queries + 1
	local queryHandler = dbQuery(dbHandler.dbConnection, ...)

	if not queryHandler then return false end

	local res, rows, lastID = dbPoll(queryHandler, -1)
	if not res or rows == 0 then return false end

	if rows == 1 then
		return res[1], rows, lastID
	else
		return res, rows, lastID
	end
end

function query(...)
	dbHandler.queries = dbHandler.queries + 1
	local queryHandler = dbQuery(dbHandler.dbConnection, ...)

	if not queryHandler then return false end
	if dbFree(queryHandler) then return true else return false end
end

function escapeString(str)
	local String = string.gsub(tostring(str),"'","")
	String = string.gsub(String,'"',"")
	String = string.gsub(String,';',"")
	String = string.gsub(String,"\\","")
	String = string.gsub(String,"/*","")
	String = string.gsub(String,"*/","")
	String = string.gsub(String,"'","")
	String = string.gsub(String,"`","")
	return String
end

function setNames()
	if dbHandler.dbConnection then
		query("SET NAMES utf8mb4_general_ci")
	end
end
setTimer(setNames, 1000 * 60 * 60, 0)
