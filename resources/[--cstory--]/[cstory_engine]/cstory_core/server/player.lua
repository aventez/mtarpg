function spawnLoggedPlayer(char)
	setElementData(client, "character:char_id", char["id"])
	setElementData(client, "character:char_skin", char["skin"])
	setElementData(client, "character:char_playtime", char["playTime"])
	setElementData(client, "character:char_name", char["name"])
	setElementData(client, "character:jointime", getRealTime().timestamp)

	setPlayerMoney(client, char["money"], true)

	setPlayerName(client, string.gsub(char["name"], " ", "_"))
	setPlayerNametagShowing(client, false)

	spawnPlayer(client, 2352.30, -21.71, 26.48, 90.0, char["skin"])
	setCameraTarget(client, client)

	outputChatBox(string.format("Witaj %s (ID: %d; UID: %d; GUID: %d [%s])", getElementData(client, "character:char_name"), getElementData(client, "character:id"), getElementData(client, "character:char_id"), getElementData(client, "global:member_id"), getElementData(client, "global:member_name")), client, 230, 178, 7)

	triggerEvent("hook_onPlayerSpawnedInGame", client)
end
addEvent("spawnLoggedPlayer", true)
addEventHandler("spawnLoggedPlayer", resourceRoot, spawnLoggedPlayer)

addEvent("hook_onPlayerSpawnedInGame")

function quitPlayer(quitType)
	savePlayer(source)
end
addEventHandler("onPlayerQuit", getRootElement(), quitPlayer)

function savePlayer(player)
	if getElementData(player, "character:char_id") then
		exports.cstory_db:queryResult("UPDATE characters SET playTime = ? WHERE id = ?", tonumber(getElementData(player, "character:char_playtime")), tonumber(getElementData(player, "character:char_id")))

		triggerEvent("hook_onPlayerSaveAfterQuit", player)
	end
end

addEvent("hook_onPlayerSaveAfterQuit")

function getPlayerByUid(uid)
	for _, v in ipairs(getElementsByType("player")) do
		if tonumber(getElementData(v, "character:char_id")) == tonumber(uid) then return v end
	end

	return false
end


function calculatePlayersOnline()
	for k, player in ipairs(getElementsByType("player")) do
		if not getElementData(player, "character:afk") and getElementData(player, "character:char_id") then
			setElementData(player, "character:char_playtime", tonumber(getElementData(player, "character:char_playtime")) + 1)
		end
	end
end
setTimer(calculatePlayersOnline, 1000, 0)


function giveMoney(player, amount)
	if not isElement(player) or amount == 0 then return end

	if amount < 0 then
		takePlayerMoney(player, -amount)
	else
		givePlayerMoney(player, amount)
	end

	exports.cstory_db:query("UPDATE characters SET money = ? WHERE id = ?", getPlayerMoney(player), getElementData(player, "character:char_id"))
end

function giveScore(player, amount)
	if not isElement(player) or amount == 0 then return end

	setElementData(player, "global:member_gamescore", getElementData(player, "global:member_gamescore") + amount)

	exports.cstory_db:query("UPDATE globals SET score = ? WHERE id = ?", getElementData(player, "global:member_gamescore"), getElementData(player, "global:member_id"))
end