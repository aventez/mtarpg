local screenW, screenH = guiGetScreenSize()
local InfoBox = {
    ['pos'] = {['x'] = screenW/2 - 200, ['y'] = screenH*0.7},
    ['show'] = false,
    ['font'] = dxCreateFont("files/myriadproregular.ttf", 12, false, "cleartype")
}

addEventHandler("onClientResourceStart", resourceRoot,
    function()
        addEventHandler("onClientRender", root, renderInfoBox)
    end
)

function outputCommandError(msg)
	outputChatBox(msg, 150, 150, 150)
end

function outputNotification(msg)
	outputChatBox(msg, 150, 150, 150)
end

function showInfoBox(msg)
	InfoBox['show'] = true
    InfoBox['show_time'] = getTickCount()
    InfoBox['msg'] = msg
    playSound("files/infobox.mp3")
end

addEvent("outputInfoBox", true)
addEventHandler("outputInfoBox", resourceRoot, showInfoBox)

function renderInfoBox()
    if InfoBox['show'] then
        dxDrawRectangle(InfoBox['pos']['x'], InfoBox['pos']['y'], 400, 120, tocolor(0, 0, 0, 190))
        dxDrawRectangle(InfoBox['pos']['x'] + 2, InfoBox['pos']['y'] + 2, 396, 20, tocolor(0, 0, 0, 80))
        dxDrawText("Powiadomienie", InfoBox['pos']['x'] + 200, InfoBox['pos']['y'] + 12, InfoBox['pos']['x'] + 200, InfoBox['pos']['y'] + 12, tocolor(255, 255, 255, 255), 0.7, InfoBox['font'], "center", "center", false, false, false, false, true)
        dxDrawText(InfoBox['msg'], InfoBox['pos']['x'], InfoBox['pos']['y'], InfoBox['pos']['x'] + 400, InfoBox['pos']['y'] + 120, tocolor(255, 255, 255, 255), 0.8, InfoBox['font'], "center", "center", false, true, false, false, true)

        if getTickCount() - InfoBox['show_time'] > 5000 then
            InfoBox['show'] = false
            InfoBox['show_time'] = 0
        end
    end
end