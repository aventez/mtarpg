local IDlist = {}

function prepare()
	for i=1,500,1 do
		IDlist[i] = false
	end
end
addEventHandler("onResourceStart", resourceRoot, prepare)

function assignID()
	for i=1,500,1 do
		if not IDlist[i] then
			IDlist[i] = true
			setElementData(source, "character:id", i)
			return
		end
	end
end
addEventHandler("onPlayerJoin", root, assignID)

function removeID()
	local id = getElementData(source, "character:id")

	IDlist[id] = false
end
addEventHandler("onPlayerQuit", root, removeID)

function getPlayerByID(id)
	local players = getElementsByType("player")

	for _, v in ipairs(players) do
		if tonumber(getElementData(v, "character:id")) == id then return v end
	end

	return false
end