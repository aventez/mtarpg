﻿local bus_stops = {
	{["name"] = "Seville", ["pos"] = {2360.30, -21.71, 26.48}, ["angle"] = 90.0},
	{["name"] = "Jefferson", ["pos"] = {2217.02, -72.93, 26.48}, ["angle"] = 180.0},
	{["name"] = "Na szmaragdowej", ["pos"] = {2230.50, -21.67, 26.48}, ["angle"] = 0.0},
	{["name"] = "Tartak", ["pos"] = {-505.56, -115.80, 64.87}, ["angle"] = 0.0},
	{["name"] = "Hurtownia", ["pos"] = {-504.37, -559.32, 25.52}, ["angle"] = 270.0},
	{["name"] = "Chilli Pub", ["pos"] = {2065.05, 49.32, 26.58}, ["angle"] = 0.0},
	{["name"] = "Lotniskowiec", ["pos"] = {1846.84, -322.25, 41.10}, ["angle"] = 0.0},
}

function RegisterActions()
	for k, v in ipairs(bus_stops) do
		exports.cstory_actions:addAction(v.object, "bus_stop", 3.0, true)
	end
end

function Register3dTexts()
	for k, v in ipairs(bus_stops) do
		bus_stops[k].textid = exports.cstory_3dtext:Create3dText(v.pos[1], v.pos[2], v.pos[3], string.format("Przystanek autobusowy #%d\n#83FF36%s", k, v.name), tocolor(255, 255, 255), 20.0, 1.4, "arial", true)
	end
end

function tocolor( r, g, b, a )
   a = tonumber( a ) or 255
return tonumber( string.format( "0x%X%X%X%X", a, r, g, b ) )
end


addEventHandler("onResourceStart", resourceRoot, function()
	for k, v in ipairs(bus_stops) do
		bus_stops[k].object = createObject(1257, v.pos[1], v.pos[2], v.pos[3], 0.0, 0.0, v.angle)
	end

	RegisterActions()
	Register3dTexts()
end)

addEventHandler("onResourceStop", resourceRoot, function()
	for k, v in ipairs(bus_stops) do
		exports.cstory_3dtext:Remove3dText(v.textid)
		destroyElement(v.object)
	end
end)


-- [[ CSTORY_ACTIONS AND CSTORY_3DTEXT FALLBACK ]]--
addEventHandler("onResourceStart", root, function( res )
	if getResourceName(res) == "cstory_actions" then RegisterActions() end
	if getResourceName(res) == "cstory_3dtext" then Register3dTexts() end
end)





function sendBusData(action_element)
	local data = {}
	data.stops = {}

	local start = 0
	for k, v in ipairs(bus_stops) do if v.object == action_element then start = k end end

	for k, v in ipairs(bus_stops) do
		if v.object ~= action_element then
			v.traveltime, v.travelcost = calculateTravelInfo(bus_stops[start], v)

			table.insert(data.stops, v)
		else
			data.info = v
		end
	end

	triggerClientEvent(client, "receiveBusData", resourceRoot, data)
end

addEvent("requestBusData", true)
addEventHandler("requestBusData", resourceRoot, sendBusData)


function onBusEntered(data, start)
	local traveltime, travelcost = calculateTravelInfo(start, data)
	takePlayerMoney(client, travelcost)

	exports.cstory_chat:sendRadiusMessage(client, string.format("** %s odjechał autobusem w kierunku %s (( %s ))", getElementData(client, "character:char_name"), data.name, getElementData(client, "character:char_name")), 20, 142, 153, 242)
end

addEvent("onClientEnterBus", true)
addEventHandler("onClientEnterBus", resourceRoot, onBusEntered)

function calculateTravelInfo(start, stop)
	local traveltime, travelcost
	traveltime = math.ceil(getDistanceBetweenPoints3D(start.pos[1], start.pos[2], start.pos[3], stop.pos[1], stop.pos[2], stop.pos[3])/200) + 5

	if getElementData(client, "character:char_playtime") < 60*60*10 then travelcost = 0
	else travelcost = math.ceil(traveltime * 0.8) end

	return traveltime, travelcost
end