﻿local busmenu = {
	display = false,
	info = nil,
	stops = nil,
	selected = nil,
	exited_meanwhile = false,
	font = dxCreateFont("files/myriadproregular.ttf", 11, false, "default")
}

function onBusStopAction(action_element, actiontype)
	if actiontype ~= "bus_stop" then return end
	if busmenu.driving then return end

	busmenu.exited_meanwhile = false

	triggerServerEvent("requestBusData", resourceRoot, action_element)
end
addEventHandler("onActionFired", localPlayer, onBusStopAction)

function onBusStopActionEnd(actiontype)
	if actiontype ~= "bus_stop" then return end
	if not busmenu.display then 
		busmenu.exited_meanwhile = true
		return 
	end

	removeEventHandler("onClientRender", root, renderBusMenu)
	removeEventHandler("onClientKey", root, onMouseClick)
	busmenu.display = false
	busmenu.info = nil
	busmenu.stops = nil
	showCursor(false)
end
addEventHandler("onActionEnd", localPlayer, onBusStopActionEnd)

local width, height = guiGetScreenSize() 

function renderBusMenu()
	local corner = { x = width/2 - 384, y = height/2 - 384 }
	local alpha = 200

	busmenu.selected = nil

	local cx, cy, _, _, _ = getCursorPosition()
	cx = cx * width
	cy = cy * height

	for k, v in ipairs(busmenu.stops) do
		if cx >= corner.x + 8 and cx <= corner.x + 165 and cy >= corner.y + 8 and cy <= corner.y + 86 then 
			alpha = 255
			busmenu.selected = k
		else alpha = 170 end

		dxDrawImage(corner.x, corner.y, 256, 128, "files/bg.png", 0, 0, 0, tocolor(255, 255, 255, alpha), false)
		dxDrawText(v.name, corner.x + 8, corner.y + 8, corner.x + 165, corner.y + 33, tocolor(40, 129, 193, alpha), 1.0, busmenu.font, "center", "bottom")

		dxDrawText(string.format("%s\nCzas przejazdu: %d sekund\nKoszt przejazdu: %s", getZoneName(v.pos[1], v.pos[2], v.pos[3]), v.traveltime, v.travelcost == 0 and "darmowy" or string.format("$%d", v.travelcost)), corner.x + 14, corner.y + 40, corner.x + 165, corner.y + 83, tocolor(255, 255, 255, alpha), 0.75, busmenu.font, "left", "top")

		corner.x = (width/2 - 384) + 256*((k)%3)
		corner.y = (height/2 - 384) + 128*math.floor(k/3)
	end
end


function onMouseClick(button, press)
	if button == "mouse1" and press then
		if busmenu.selected then
			if getPlayerMoney(localPlayer) < busmenu.stops[busmenu.selected].travelcost then
				exports.cstory_core:outputNotification("Nie posiadasz wystarczającej ilości pieniędzy")
				cancelEvent()
				return
			end

			triggerServerEvent("onClientEnterBus", resourceRoot, busmenu.stops[busmenu.selected], busmenu.info)
			busmenu.driving = true
			fadeCamera(false, 1.5)
			toggleAllControls(false)
			setTimer(function()
				setElementInterior(localPlayer, 1, 1.808619, 32.384357, 1199.593750)
			end, 2500, 1)

			
			setTimer(function (bus)
				setElementInterior(localPlayer, 0, bus.pos[1], bus.pos[2], bus.pos[3])
				fadeCamera(true, 1.5)
				toggleAllControls(true)
                busmenu.driving = false
			end, busmenu.stops[busmenu.selected].traveltime*1000, 1, busmenu.stops[busmenu.selected])

			onBusStopActionEnd()
			cancelEvent()
		end
	end
end




function receiveBusData(data)
	if busmenu.exited_meanwhile then return end
	busmenu.display = true
	busmenu.info = data.info
	busmenu.stops = data.stops
	showCursor(true)

	addEventHandler("onClientRender", root, renderBusMenu)
	addEventHandler("onClientKey", root, onMouseClick)
end

addEvent("receiveBusData", true)
addEventHandler("receiveBusData", resourceRoot, receiveBusData)