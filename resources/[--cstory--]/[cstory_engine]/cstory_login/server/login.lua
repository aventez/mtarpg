function handleLoginRequest(login, pass)
	local result = exports.cstory_db:queryResult("SELECT * FROM globals WHERE name = ?", login)
	if not result then
		triggerClientEvent(client, "onServerLoginRequestResponse", resourceRoot, false, "Użytkownik o podanym loginie nie istnieje")
	else
		local uid = result['id']
		result = exports.cstory_db:queryResult("SELECT * FROM globals WHERE id = ? AND pass = MD5(?)", uid, pass)
		
		if not result then
			triggerClientEvent(client, "onServerLoginRequestResponse", resourceRoot, false, "Podane przez Ciebie hasło nie jest poprawne")
		else
			-- zalogowany
			setElementData(client, "global:member_id", uid)
			setElementData(client, "global:member_name", result['name'])
			setElementData(client, "global:member_admin_level", result['adminLevel'])
			setElementData(client, "global:member_gamescore", result['score'])

			local chars, rows = exports.cstory_db:queryResult("SELECT * FROM characters WHERE globalId = ?", uid)

			if rows == 1 then chars = {chars} end

			triggerClientEvent(client, "onServerLoginRequestResponse", resourceRoot, true, "Zalogowano", chars)
		end
	end

end
addEvent("onPlayerLoginRequest", true)
addEventHandler("onPlayerLoginRequest", resourceRoot, handleLoginRequest)