local root = getRootElement()
local resourceRoot = getResourceRootElement(getThisResource())
local screenWidth, screenHeight = guiGetScreenSize()

local LoginBox = {
    label = {}
}

local CharsBox = {
    selected_char = 0
}

local data = { showed=nil, buttonHover={zaloguj=false,rejestruj=false}, info=nil, misc=nil, step=0, stepBegin=0, input_showed = false, nickEditBox, passEditBox, windowsPos={}, rememberData = false }

function isMouseIn(psx,psy,pssx,pssy)
    if not isCursorShowing() then return false end
    local absoluteX, absoluteY = getCursorPosition()
    absoluteX = absoluteX * screenWidth
    absoluteY = absoluteY * screenHeight
    
    if absoluteX >= psx and absoluteX <= pssx and absoluteY >= psy and absoluteY <= pssy then return true end

    return false
end

function checkRememberPassword()
    local pass_file = xmlLoadFile ( "rememberPassword.xml" ) --Attempt to load the xml file 
    
    if pass_file then
        local node = xmlFindChild( pass_file, "login", 0 )
        local success = xmlNodeGetValue ( node ) -- Get the value of it
        if success then -- Check if it was successful
            guiSetText(data.nickEditBox, tostring(success))
        end

        node = xmlFindChild( pass_file, "password", 0 )
        success = xmlNodeGetValue ( node ) -- Get the value of it
        if success then -- Check if it was successful
            guiSetText(data.passEditBox, tostring(success))
        end
        data.rememberData = true

        xmlUnloadFile(pass_file)
    end

end
function renderLoginBox()
    if data.step == 0 then  
        local progress = (getTickCount() - data.stepBegin) / 5000
        local alpha = 255 * progress
        if progress > 1 then
            progress = 1
            alpha = 255

            showCursor(true)
        end

        data.windowPos = {x = screenWidth/2 - 300, y = screenHeight/2 - 125}

        dxDrawImage(data.windowPos.x, data.windowPos.y, 600, 250, "files/logowanie.png", 0, 0, 0, tocolor(255, 255, 255, alpha), false)
        
        --[[if isMouseIn(data.windowPos.x + 175, data.windowPos.y + 251, data.windowPos.x + 323, data.windowPos.y + 280) and data.input_showed then 
            dxDrawImage(data.windowPos.x + 146, data.windowPos.y + 246, 210, 57, "files/rejestruj_podciemniona.png", 0, 0, 0, tocolor(255, 255, 255, alpha), false)
        else 
            dxDrawImage(data.windowPos.x + 146, data.windowPos.y + 246, 210, 57, "files/ikona_rejestruj.png", 0, 0, 0, tocolor(255, 255, 255, alpha), false) 
        end

        if isMouseIn(data.windowPos.x + 175, data.windowPos.y + 287, data.windowPos.x + 323, data.windowPos.y + 316) and data.input_showed then 
            dxDrawImage(data.windowPos.x + 146, data.windowPos.y + 282, 210, 57, "files/zaloguj_podciemniona.png", 0, 0, 0, tocolor(255, 255, 255, alpha), false)
        else 
            dxDrawImage(data.windowPos.x + 146, data.windowPos.y + 282, 210, 57, "files/ikona_zaloguj.png", 0, 0, 0, tocolor(255, 255, 255, alpha), false) 
        end--]]

        if data.rememberData then dxDrawImage(data.windowPos.x + 41, data.windowPos.y + 164, 47, 37, "files/ikona_ptaszek.png", 0, 0, 0, tocolor(255, 255, 255, alpha), false) end
        
        if progress == 1 and not data.input_showed then
            data.nickEditBox = guiCreateEdit(data.windowPos.x + 254, data.windowPos.y + 58, 254, 31, "", false)
            data.passEditBox = guiCreateEdit(data.windowPos.x + 254, data.windowPos.y + 120, 254, 31, "", false)
            guiSetProperty(data.nickEditBox, "Alpha", 0);
            guiSetProperty(data.passEditBox, "Alpha", 0);

            checkRememberPassword()

            guiSetInputMode ("no_binds_when_editing")

            data.input_showed = true
        end

        
        if data.input_showed then
            if data.info ~= nil then
                dxDrawText(data.info, data.windowPos.x + 254, data.windowPos.y + 220, data.windowPos.x + 584, data.windowPos.y + 242, tocolor(213, 0, 0, 255), 1, "default", "left", "center", true, false)
            end

            dxDrawText(guiGetText(data.nickEditBox), data.windowPos.x + 260, data.windowPos.y + 58, data.windowPos.x + 508, data.windowPos.y + 89, tocolor(0, 0, 0, 200), 1, "default", "left", "center", true, false)
            dxDrawText(string.rep("*",string.len(guiGetText(data.passEditBox))), data.windowPos.x + 260, data.windowPos.y + 120, data.windowPos.x + 508, data.windowPos.y + 151, tocolor(0, 0, 0, 255), 0.9, "default", "left", "center", true, false)
        end
    end
end



function loginBoxClick(btn,state)
    if btn=="left" and state=="down" and data.input_showed then
        if isMouseIn(data.windowPos.x + 60, data.windowPos.y + 176, data.windowPos.x + 76, data.windowPos.y + 192) then
            if data.rememberData then data.rememberData = false else data.rememberData = true end
        elseif isMouseIn(data.windowPos.x + 175, data.windowPos.y + 251, data.windowPos.x + 323, data.windowPos.y + 280) then
            local login = guiGetText(data.nickEditBox)
            local pass = guiGetText(data.passEditBox)
            if string.len(login) > 22 or string.len(pass) > 22 and string.len(login) < 4 or string.len(pass) < 4 then
                data.info="Login/Hasło muszą mieć mniej niż od 3 do 22 znaków."
                setTimer(function() data.info=nil end, 3000, 1)
                return
            end
            --triggerServerEvent("logging:newAccount", resourceRoot, login, pass)
        elseif isMouseIn(data.windowPos.x + 326, data.windowPos.y + 171, data.windowPos.x + 420, data.windowPos.y + 200) then
            local login = guiGetText(data.nickEditBox)
            local pass = guiGetText(data.passEditBox)
            if string.len(login) < 4 or string.len(pass) < 4 then
                data.info = "Wypełnij wszystkie pola!"
                setTimer(function() data.info=nil end, 3000, 1)
                return
            end
            triggerServerEvent("onPlayerLoginRequest", resourceRoot, login, pass)
        end
    end
end

addEventHandler("onClientClick", root, loginBoxClick)


function playerLoaded()
    local screenW, screenH = guiGetScreenSize()
    CharsBox.window = guiCreateGridList((screenW - 636) / 2, (screenH - 321) / 2, 636, 321, false)
    guiSetAlpha(CharsBox.window, 0.73)

    CharsBox.label = guiCreateLabel(0.38, 0.03, 0.24, 0.07, "Wybierz postać", true, CharsBox.window)
    guiSetFont(CharsBox.label, "clear-normal")
    guiLabelSetHorizontalAlign(CharsBox.label, "center", false)
    guiLabelSetVerticalAlign(CharsBox.label, "center")

    guiSetVisible(CharsBox.window, false)
    

	addEventHandler("onClientRender", root, renderBlur)
	toggleAllControls(false, true, false)
	showChat(false)
	fadeCamera(true)
	cameraOn()

	data.stepBegin = getTickCount()
    data.step = 0
    data.showed = true
    addEventHandler("onClientRender", root, renderLoginBox)
end
addEventHandler("onClientResourceStart", resourceRoot, playerLoaded)


function handleLoginRequestResponse(state, msg, chars)
    if not state then
        data.info=tostring(msg)
        setTimer(function() data.info=nil end, 3000, 1)
    else
        if data.rememberData then
            local pass_file = xmlLoadFile ( "rememberPassword.xml" ) --Attempt to load the xml file 
            if not pass_file then
                pass_file = xmlCreateFile ( "rememberPassword.xml", "root" )
                xmlNodeSetValue(xmlCreateChild( pass_file, "login"), guiGetText(data.nickEditBox))
                xmlNodeSetValue(xmlCreateChild( pass_file, "password"), guiGetText(data.passEditBox))
                xmlSaveFile( pass_file ) --Save the xml from memory for use next time
                xmlUnloadFile( pass_file )
            end
        else
            local pass_file = xmlLoadFile ( "rememberPassword.xml" ) --Attempt to load the xml file 
            if pass_file then
                xmlUnloadFile( pass_file)
                fileDelete("rememberPassword.xml")
            end
        end
        
        -- Usuwamy elementy
        destroyElement(data.nickEditBox)
        destroyElement(data.passEditBox)
        removeEventHandler("onClientRender", root, renderLoginBox)
        removeEventHandler("onClientClick", root, loginBoxClick)

        CharsBox.chars = chars

        showCharSelection()
    end
end
addEvent("onServerLoginRequestResponse", true)
addEventHandler("onServerLoginRequestResponse", resourceRoot, handleLoginRequestResponse)

function characterSelect(button,press) 
        if button == "mouse1" then
            if CharsBox.selected_char > 0 then
                -- spawn char

                removeEventHandler("onClientRender", root, drawCharacters)
                removeEventHandler("onClientKey", root, characterSelect)
                guiSetVisible(CharsBox.window, false)

                cameraOff()
                removeEventHandler("onClientRender", root, renderBlur)
                showChat(true)
                showCursor(false)
                fadeCamera(false)
                setElementAlpha(localPlayer,255)
                toggleAllControls(true, true, true)
                setPlayerHudComponentVisible("all", false)

                for i=1,50,1 do
                    outputChatBox(" ")
                end

                setTimer(function ()
                    fadeCamera(true)
                    setPlayerHudComponentVisible("all", true)
                    setPlayerHudComponentVisible("area_name", false)
                    setPlayerHudComponentVisible("vehicle_name", false)
                    triggerServerEvent("spawnLoggedPlayer", root, CharsBox.chars[CharsBox.selected_char])
                end, 2000, 1)       
            end
        end
end 

function showCharSelection()
    guiSetVisible(CharsBox.window, true)
    addEventHandler("onClientRender", root, drawCharacters)

    addEventHandler("onClientKey", root, characterSelect)
end

function drawCharacters()
    local screenW, screenH = guiGetScreenSize()

    local x = (screenW - 636) / 2 + 20
    local y = (screenH - 321) / 2 + 40

    local selected = false

    for k, v in ipairs(CharsBox.chars) do
        if k % 3 == 1 then x = (screenW - 636) / 2 + 12
        elseif k % 3 == 2 then x = (screenW - 636) / 2 + 12 + 206
        elseif k % 3 == 0 then x = (screenW - 636) / 2 + 12 + 412 end

        local cx, cy, _, _, _ = getCursorPosition()
        cx = cx * screenW
        cy = cy * screenH

        if (cx >= x and cx <= x + 200) and (cy >= y and cy <= y + 62) then
            dxDrawRectangle(x, y, 200, 62, tocolor(0, 0, 0, 180), true)
            dxDrawRectangle(x-2, y, 204, 1, tocolor(187, 232, 139, 200), true)

            CharsBox.selected_char = k
            selected = true
        else
            dxDrawRectangle(x, y, 200, 62, tocolor(0, 0, 0, 100), true)
        end

        dxDrawImage(x + 2, y + 2, 58, 58, 'files/skins/' .. v["skin"] .. '.png', 0, 0, 0, tocolor(255,255,255,255), true)
        dxDrawText(v["name"], x + 70, y + 10, x + 140, y + 30, tocolor(255, 255, 255, 255), 1, "Tahoma", "left", "top", false, false, true)

        local h = math.floor(v["playTime"]/3600)
        local m = math.floor((v["playTime"] - h*3600) / 60)

        dxDrawText(h .. "h " .. m .. "min online", x + 72, y + 28, x + 140, y + 44, tocolor(255, 255, 255, 255), 0.8, "default", "left", "top", false, false, true)

        if k % 3 == 0 then y = y + 70 end
    end

    if not selected then CharsBox.selected_char = 0 end
end