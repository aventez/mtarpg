local screenWidth, screenHeight = guiGetScreenSize()
local myScreenSource = dxCreateScreenSource(screenWidth, screenHeight)
local blurShader, blurTec = dxCreateShader("files/BlurShader.fx")

function renderBlur()
    dxUpdateScreenSource(myScreenSource)
    
    dxSetShaderValue(blurShader, "ScreenSource", myScreenSource);
    dxSetShaderValue(blurShader, "BlurStrength", 10);
	dxSetShaderValue(blurShader, "UVSize", screenWidth, screenHeight);

    dxDrawImage(0, 0, screenWidth, screenHeight, blurShader)
end