
RepairGUI = {
    vehicle = nil,
    panels = {},
    doors = {},
    health = nil,
    window = nil
}
addEventHandler("onClientResourceStart", resourceRoot,
    function()
        local screenW, screenH = guiGetScreenSize()
        RepairGUI.window = guiCreateWindow((screenW - 411) / 2, (screenH - 370) / 2, 411, 370, "Naprawa", false)
        guiWindowSetSizable(RepairGUI.window, false)

        RepairGUI.health = guiCreateButton(10, 28, 391, 53, "Uszkodzenia mechaniczne", false, RepairGUI.window)
        RepairGUI.panel = guiCreateTabPanel(9, 103, 392, 257, false, RepairGUI.window)

        RepairGUI.tab = guiCreateTab("Uszkodzenia karoserii", RepairGUI.panel)
        RepairGUI.tab2 = guiCreateTab("Uszkodzenia drzwi", RepairGUI.panel)

        RepairGUI.panels[6] = guiCreateButton(135, 10, 123, 41, "Zderzak przód", false, RepairGUI.tab)
        RepairGUI.panels[7] = guiCreateButton(135, 181, 123, 41, "Zderzak tył", false, RepairGUI.tab)
        RepairGUI.panels[5] = guiCreateButton(135, 61, 123, 41, "Szyba czołowa", false, RepairGUI.tab)
        RepairGUI.panels[1] = guiCreateButton(6, 34, 123, 45, "Nadkole\nlewy przód", false, RepairGUI.tab)
        RepairGUI.panels[2] = guiCreateButton(262, 34, 123, 45, "Nadkole\nprawy przód", false, RepairGUI.tab)
        RepairGUI.panels[4] = guiCreateButton(262, 136, 123, 45, "Nadkole\nprawy tył", false, RepairGUI.tab)
        RepairGUI.panels[3] = guiCreateButton(6, 136, 123, 45, "Nadkole\nlewy tył", false, RepairGUI.tab)

        RepairGUI.doors[1] = guiCreateButton(135, 10, 123, 41, "Klapa silnika", false, RepairGUI.tab2)
        RepairGUI.doors[2] = guiCreateButton(135, 181, 123, 41, "Klapa bagażnika", false, RepairGUI.tab2)
        RepairGUI.doors[3] = guiCreateButton(6, 34, 123, 45, "Drzwi\nlewy przód", false, RepairGUI.tab2)
        RepairGUI.doors[4] = guiCreateButton(262, 34, 123, 45, "Drzwi\nprawy przód", false, RepairGUI.tab2)
        RepairGUI.doors[6] = guiCreateButton(262, 136, 123, 45, "Drzwi\nprawy tył", false, RepairGUI.tab2)
        RepairGUI.doors[5] = guiCreateButton(6, 136, 123, 45, "Drzwi\nlewy tył", false, RepairGUI.tab2)

        local w = guiGetSize(RepairGUI.window, false)
        local width = dxGetTextWidth("zamknij", 1, "default")
        local label = guiCreateLabel("right" == "left" and 5 or w - 5 - width, 2, width, 15, "zamknij", false, RepairGUI.window)
        
        guiLabelSetColor(label, 160, 160, 160)
        guiLabelSetHorizontalAlign(label, "center", false)
        guiSetProperty(label, "ClippedByParent", "False")
        guiSetProperty(label, "AlwaysOnTop", "True")

        addEventHandler("onClientGUIClick", label, 
            function(button, state)
                if button == "left" and state == "up" then
                    -- back
                    guiSetVisible(RepairGUI.window, false)
                    showCursor(false)
                end
            end, 
        false)

        addEventHandler("onClientMouseEnter", label, function() guiLabelSetColor(label, 255, 69, 59) end, false)
        addEventHandler("onClientMouseLeave", label, function() guiLabelSetColor(label, 160, 160, 160) end, false)

        addEventHandler("onClientGUIClick", RepairGUI.window, 
        function(button, state)
            if button == "left" and state == "up" then
                if source == RepairGUI.health then
                    triggerServerEvent("onEngineerRepair", resourceRoot, RepairGUI.vehicle, "hp")
                end

                for i=1,7,1 do
                    if source == RepairGUI.panels[i] then
                        triggerServerEvent("onEngineerRepair", resourceRoot, RepairGUI.vehicle, "panels", i-1)
                    elseif source == RepairGUI.doors[i] then
                        triggerServerEvent("onEngineerRepair", resourceRoot, RepairGUI.vehicle, "doors", i-1)
                    end
                end
            end
        end)

        guiSetVisible(RepairGUI.window, false)
    end
)

function handleEngineerRepair(vehicle)
    RepairGUI.vehicle = vehicle

    guiSetText(RepairGUI.window, "Naprawa ("..getVehicleName(vehicle)..")")

    guiSetVisible(RepairGUI.window, true)
    showCursor(true)
end
addEvent("showEngineerRepair", true)
addEventHandler("showEngineerRepair", resourceRoot, handleEngineerRepair)
