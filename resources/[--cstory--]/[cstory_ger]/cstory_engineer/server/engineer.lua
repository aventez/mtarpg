local MAX_WARSZTATY = 100
local Warsztaty = {}

function aEngineerCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") or not getElementData(source, "character:adminDuty") or getElementData(source, "global:member_admin_level") < 2 then return end

	local arg = {...}

	local sub = arg[1] or ""

	if sub == "dajprace" then
		targetid = tonumber(arg[2]) or 0
		targetid = tonumber(targetid)
		if not targetid or targetid < 1 then return exports.cstory_core:outputCommandError("Tip: /amech dajprace [id gracza]", source) end
		local target = exports.cstory_id:getPlayerByID(targetid)
		if not target then return exports.cstory_core:outputCommandError("Gracz o podanym id nie istnieje.", source) end
		
		setElementData(target, "character:is_engineer", true);
		exports.cstory_core:outputCommandError("Gracz o podanym id został mechanikiem", source)
		exports.cstory_core:outputCommandError("Zostałeś mechanikiem", target)
	elseif sub == "stworz-warsztat" then
			local x,y,z = getElementPosition(source)
			local area = createColSphere(x, y, z, 10.0)

			local warsztat_id = nil
			for i=1,MAX_WARSZTATY,1 do
				if Warsztaty[i] == false then
					Warsztaty[i] = {area, nil}
					warsztat_id = i
					break
				end
			end

			if warsztat_id then
				exports.cstory_core:outputCommandError("Utworzyłeś warsztat o id " .. warsztat_id, source)
				Warsztaty[warsztat_id][2] = exports.cstory_3dtext:Create3dText(x, y, z, "Warsztat samochodowy\n(id: "..warsztat_id..")", 0xFF0000FF, 10.0)
			end
	end
end
addCommandHandler("amech", aEngineerCommand)


function repairCommand(source, cmd, ...)
	if not getElementData(source, "character:is_engineer") then return exports.cstory_core:outputCommandError("Nie jesteś mechanikiem", source) end

	local warsztat = false

	for i=1,MAX_WARSZTATY,1 do
		if not Warsztaty[i] == false then
			if(isElementWithinColShape(source, Warsztaty[i][1])) then
				warsztat = i
				break
			end
		end
	end

	if not warsztat then return exports.cstory_core:outputCommandError("Nie znajdujesz się w warsztacie", source) end

	local vehicles = getElementsByType("vehicle")
	local pretender_vehicle = nil
	local pretender_distance = 10.0
	local x,y,z = getElementPosition(source)
	for i=1,#vehicles,1 do
		if isElementWithinColShape(vehicles[i], Warsztaty[warsztat][1]) then
			local vx,vy,vz = getElementPosition(vehicles[i])

			local distance = getDistanceBetweenPoints3D(x,y,z,vx,vy,vz)
			if distance < pretender_distance then 
				pretender_vehicle = vehicles[i]
				pretender_distance = distance
			end
		end
	end

	if not pretender_vehicle then return exports.cstory_core:outputCommandError("W pobliżu nie znajduje się żaden pojazd", source) end

	triggerClientEvent(source, "showEngineerRepair", resourceRoot, pretender_vehicle)
end
addCommandHandler("naprawa", repairCommand)


function engineerGuiResponse(vehicle, type, data)
	if type == "hp" then
		setElementHealth(vehicle, 1000)
	elseif type == "panels" then
		setVehiclePanelState(vehicle, data, 0)
	elseif type == "doors" then
		setVehicleDoorState(vehicle, data, 0)
	end
end

addEvent("onEngineerRepair", true)
addEventHandler("onEngineerRepair", resourceRoot, engineerGuiResponse)


function prepare()
	for i=1,MAX_WARSZTATY,1 do
		Warsztaty[i] = false
	end
end
addEventHandler("onResourceStart", resourceRoot, prepare)