-- objekt 654 - 661
-- -491.54, -132.93, 69.28
pickup = {}

function createTree(x, y, z)
	local tree = createElement("tree")

	setElementData(tree, "object", createObject(654, x, y, z-1.5))
	setElementData(tree, "health", 100.0)
	setElementPosition(tree, x, y, z)
end

function hihi()
	createTree(-495.46, -126.83, 67.36)
	createTree(-491.54, -132.93, 69.28)
	createTree(-480.67, -124.92, 66.29)
	createTree(-486.03, -117.49, 64.68)
	createTree(-494.38, -121.00, 65.85)
	createTree(-497.72, -112.37, 64.39)
	createTree(-491.42, -105.39, 63.03)
	createTree(-482.61, -97.04, 61.37)
	createTree(-494.14, -96.63, 61.79)
	createTree(-474.65, -101.06, 61.65)
	createTree(-480.86, -111.13, 63.47)
	createTree(-474.65, -115.74, 63.92)


	pickup.object = createPickup(-518.61, -101.01, 63.26, 3, 1239, 1)  
	pickup.area = createColCircle(-518.61, -101.01, 3.0)

	addEventHandler("onColShapeHit", pickup.area, startJob)
end

function startJob(player)
    if getElementType(player) == "player" then
        triggerClientEvent(player, "openLumberjackGui", player)
    end
end

addEventHandler("onResourceStart", resourceRoot, hihi)

function lumberjackGuiResponse(state)
	if state then
		setElementData(client, "character:tree_cuting", true)
		giveWeapon(client, 9)
		setPedWeaponSlot(client, 1)
	else
		setElementData(client, "character:tree_cuting", false)
		takeWeapon(client, 9)
	end
end

addEvent("onLumberjackGuiResponse", true)
addEventHandler("onLumberjackGuiResponse", resourceRoot, lumberjackGuiResponse)

function treeCutDown(tree)
	local x, y, z = getElementPosition(getElementData(tree, "object"))
	setElementAlpha(getElementData(tree, "object"), 0)
	setElementData(tree, "objectf", createObject(831, x, y, z+0.7))
	setElementData(tree, "regrowth", getRealTime().timestamp + 30)
end

addEvent("onTreeCutDown", true)
addEventHandler("onTreeCutDown", resourceRoot, treeCutDown)


function regrowthing()
	for k, v in ipairs(getElementsByType("tree")) do
		if getElementData(v, "health") <= 0 then
			if getElementData(v, "regrowth") and getElementData(v, "regrowth") <= getRealTime().timestamp then
				local x, y, z = getElementPosition(getElementData(v, "object"))
				destroyElement(getElementData(v, "objectf"))
				setElementData(v, "health", 100.0)
				setElementAlpha(getElementData(v, "object"), 255)
				removeElementData(v, "regrowth")
			end
		end
	end
end

setTimer(regrowthing, 1000, 0)