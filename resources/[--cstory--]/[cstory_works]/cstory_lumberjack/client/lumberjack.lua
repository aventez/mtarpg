﻿
Lumberjack = {}
addEventHandler("onClientResourceStart", resourceRoot, function()
	local screenW, screenH = guiGetScreenSize()
    Lumberjack.window = guiCreateWindow((screenW - 272) / 2, (screenH - 247) / 2, 272, 247, "Praca drwala", false)
    guiWindowSetMovable(Lumberjack.window, false)
    guiWindowSetSizable(Lumberjack.window, false)

    Lumberjack.button = guiCreateButton(35, 180, 202, 42, "Zacznij pracę", false, Lumberjack.window)

   

    local w = guiGetSize(Lumberjack.window, false)
    local width = dxGetTextWidth("zamknij", 1, "default")
    local label = guiCreateLabel("right" == "left" and 5 or w - 5 - width, 2, width, 15, "zamknij", false, Lumberjack.window)
    
    guiLabelSetColor(label, 160, 160, 160)
    guiLabelSetHorizontalAlign(label, "center", false)
    guiSetProperty(label, "ClippedByParent", "False")
    guiSetProperty(label, "AlwaysOnTop", "True")

    addEventHandler("onClientGUIClick", label, 
        function(button, state)
            if button == "left" and state == "up" then
                -- back
                guiSetVisible(Lumberjack.window, false)
                showCursor(false)
            end
        end, 
    false)

    addEventHandler("onClientMouseEnter", label, function() guiLabelSetColor(label, 255, 69, 59) end, false)
    addEventHandler("onClientMouseLeave", label, function() guiLabelSetColor(label, 160, 160, 160) end, false)

    addEventHandler("onClientGUIClick", Lumberjack.button, 
        function(button, state)
            if button == "left" and state == "up" then
                if not getElementData(localPlayer, "character:tree_cuting") then
					guiSetText(Lumberjack.button, "Zakończ pracę")
					triggerServerEvent("onLumberjackGuiResponse", resourceRoot, true)

				else
					guiSetText(Lumberjack.button, "Zacznij pracę")
					triggerServerEvent("onLumberjackGuiResponse", resourceRoot, false)
				end



                guiSetVisible(Lumberjack.window, false)
                showCursor(false)
            end
        end, 
    false)

     guiSetVisible(Lumberjack.window, false)
end)


function openGui()
	guiSetVisible(Lumberjack.window, true)
	showCursor(true)
end


addEvent("openLumberjackGui", true)
addEventHandler("openLumberjackGui", localPlayer, openGui)
