local controlAnim = false
local cutedTree = nil
local chainsawSound = nil

function onMouseClick(button, press)
	if not getElementData(localPlayer, "character:tree_cuting") then return end

	if button == "mouse1" and press and getPedWeapon(localPlayer) == 9 then

		-- szukamy czy przed nami jest drzewo
		local x, y, z = getElementPosition(localPlayer) 
	    local r = getElementRotation(localPlayer) 
	    x = x - math.sin(math.rad(r)) * 0.5
	    y = y + math.cos(math.rad(r)) * 0.5

	    local pretender = nil
	    local pretender_distance = 2.0
	    for k, v in ipairs(getElementsByType("tree")) do
	    	if getElementData(v, "health") > 0.0 and not getElementData(v, "is_cuted") then
		    	local tx, ty, _ = getElementPosition(v)

		    	local distance = getDistanceBetweenPoints2D(x, y, tx+0.2, ty-0.2)

		    	if distance < pretender_distance then
			    	pretender_distance = distance
			    	pretender = v
		   		end
	   		end
	    end

	    if pretender then
	    	cutedTree = pretender
	    	setElementData(localPlayer, "character:cuted_tree", cutedTree)
	    	setElementData(cutedTree, "is_cuted", true)
			setElementFrozen(localPlayer, true)
			setPedAnimation(localPlayer, "chainsaw", "csaw_2", -1, false, true)  -- casaw_2 najlepsze
			controlAnim = true
			setElementData(localPlayer, "character:is_cutting", true)

			chainsawSound = playSound('files/chainsaw.mp3')
		end
	elseif button == "mouse1" and not press then
		if cutedTree then
			controlAnim = false
			setElementData(cutedTree, "is_cuted", false)
			setPedAnimation(localPlayer, false)
			setElementFrozen(localPlayer, false)
			cutedTree = false
			setElementData(localPlayer, "character:is_cutting", false)
			setElementData(localPlayer, "character:cuted_tree", nil)
			stopSound(chainsawSound)
		end
	end

	if button == "mouse1" and getPedWeapon(localPlayer) == 9 then cancelEvent() end
end

local lastDamage = 0

function renderTreeCut()
	local px, py, pz = getElementPosition(localPlayer)
	if getDistanceBetweenPoints3D(px, py, pz, -518.61, -101.01, 63.26) < 200 then
		renderLumberjacks()

		if not getElementData(localPlayer, "character:tree_cuting") then return end

		renderCutedTreesInfo()

		if controlAnim then
			if getElementData(cutedTree, "health") == 0.0 then 

				triggerServerEvent("onTreeCutDown", resourceRoot, cutedTree)

				setElementData(cutedTree, "is_cuted", false)
				controlAnim = false
				setPedAnimation(localPlayer, false)
				setElementFrozen(localPlayer, false)
				cutedTree = nil
				setElementData(localPlayer, "character:is_cutting", false)
				setElementData(localPlayer, "character:cuted_tree", nil)
				stopSound(chainsawSound)
				return
			end

			if getTickCount() - lastDamage > 20 then
				lastDamage = getTickCount()

				setElementData(cutedTree, "health", getElementData(cutedTree, "health") - 0.5)
			end

			local progress = 0.35 + ((1 - (getElementData(cutedTree, "health")/100)) * 0.1)

			local tx, ty, tz = getElementPosition(cutedTree)
			local x, y = getScreenFromWorldPosition(tx+0.2, ty-0.2, tz)

			if x and y then
				dxDrawRectangle(x-60, y-10, 120, 20, tocolor(0, 0, 0, 180))
				dxDrawRectangle(x-60, y-10, 120*(getElementData(cutedTree, "health")/100), 20, tocolor(0, 255, 0, 255))
			end

			setPedAnimationProgress(localPlayer, "csaw_2", progress)
		end
	end
end

function renderCutedTreesInfo()
	local px, py, pz = getElementPosition(localPlayer)
	local x, y, z, size, alpha, text, distance


	for k, v in ipairs(getElementsByType("tree")) do
		if getElementData(v, "regrowth") then
			local obj = getElementData(v, "object")
			if isElementOnScreen(obj) then
				x, y, z = getElementPosition(v)
				distance = getDistanceBetweenPoints3D(px, py, pz, x, y, z)
				if distance <= 10.0 then

					size = (10.0-distance)/10.0
					alpha = 200 * size
					if size < 0.7 then size = 0.7 end

					x, y = getScreenFromWorldPosition(x+0.2, y-0.2, z)

					if x and y then
						dxDrawRectangle(x-40, y-4, 80, 8, tocolor(0, 0, 0, 180))
						dxDrawRectangle(x-40, y-4, 80* (1 -((getElementData(v, "regrowth")-getRealTime().timestamp)/30)), 8, tocolor(255, 213, 74, 255))
					end
				end
			end
		end
	end
end

local ChainsawSounds = {}

function renderLumberjacks()
	for k, v in ipairs(getElementsByType("player")) do
		if isElementOnScreen(v) then
			if getElementData(v, "character:is_cutting") and getElementData(v, "character:cuted_tree") and v ~= localPlayer then
				local s1, s2 = getPedAnimation(v)
				if s2 ~= "csaw_2" then 
					setPedAnimation(v, "chainsaw", "csaw_2", -1, false, true) 
					setElementFrozen(v, true)
				end
				local progress = 0.35 + ((1 - (getElementData(getElementData(v, "character:cuted_tree"), "health")/100)) * 0.1)
				setPedAnimationProgress(v, "csaw_2", progress)
			elseif not getElementData(v, "character:is_cutting") and v ~= localPlayer and getElementData(v, "character:tree_cuting") then
				local s1, s2 = getPedAnimation(v)
				if s2 == "csaw_2" then 
					setPedAnimation(v, false)
					setElementFrozen(v, false)
				end
			end
		end

		if getElementData(v, "character:is_cutting") and getElementData(v, "character:cuted_tree") and v ~= localPlayer then
			if not ChainsawSounds[v] then ChainsawSounds[v] = playSound3D('files/chainsaw.mp3', getElementPosition(v)) end
		elseif not getElementData(v, "character:is_cutting") and v ~= localPlayer and getElementData(v, "character:tree_cuting") then
			if ChainsawSounds[v] then
				stopSound(ChainsawSounds[v])
				ChainsawSounds[v] = nil
			end
		end
	end
end

function disableChainsawSwitchOff(prevSlot, newSlot)
	if getElementData(localPlayer, "character:is_cuting") and getPedWeapon(localPlayer, newSlot) ~= 9 then
		cancelEvent()
		setPedWeaponSlot(localPlayer, 1)
	end
end


addEventHandler("onClientRender", root, renderTreeCut)
addEventHandler("onClientKey", root, onMouseClick)
addEventHandler("onClientPlayerWeaponSwitch", root, disableChainsawSwitchOff)