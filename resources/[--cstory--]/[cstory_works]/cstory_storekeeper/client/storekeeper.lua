Warehouse = {pickup_location_text = {-554.66, -525.89, 27.36}}

function startStorekeeping()
	if getElementData(localPlayer, "character:is_storekeeping") then
		setElementData(localPlayer, "character:is_storekeeping", false)

		outputChatBox("STOP PRACY MAGAZYN")
	else
		setElementData(localPlayer, "character:is_storekeeping", true)

		outputChatBox("START PRACY MAGAZYN")
	end
end

addEvent("startStorekeeping", true)
addEventHandler("startStorekeeping", resourceRoot, startStorekeeping)

function renderWarehouse()
	if not getElementData(localPlayer, "character:is_storekeeping") then return end

	local x, y = getScreenFromWorldPosition(Warehouse.pickup_location_text[1], Warehouse.pickup_location_text[2], Warehouse.pickup_location_text[3])

	if x and y then
		dxDrawText("Punkt odbioru\npaczek", x, y, x, y, tocolor(255, 255, 255, 255), 4.0, "arial", "center", "center", false, false, false, true, true)
	end	
end

addEventHandler("onClientRender", root, renderWarehouse)