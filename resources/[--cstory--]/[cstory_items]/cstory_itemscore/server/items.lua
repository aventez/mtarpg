ITEM_OWNER_PLAYER = 1

function onPlayerLogged()
	sendAllPlayerItems(source)
end
addEventHandler("hook_onPlayerSpawnedInGame", root, onPlayerLogged)

function onClientRequestItems()
	if not getElementData(client, "character:char_id") then return end
	sendAllPlayerItems(client)
end
addEvent("onClientRequestItems", true)
addEventHandler("onClientRequestItems", resourceRoot, onClientRequestItems)


function sendAllPlayerItems(player)
	local result, rows = exports.cstory_db:queryResult("SELECT * FROM items WHERE ownerType = ? AND owner = ?", ITEM_OWNER_PLAYER, getElementData(player, "character:char_id"))

	if rows and rows > 1 then
		if rows == 1 then
			result = {result}
		end

		triggerClientEvent(player, "onPlayerItemsReceived", resourceRoot, result)
	end
end

function sendPlayerItem(player, item_uid)
	local result = exports.cstory_db:queryResult("SELECT * FROM items WHERE id = ?", tonumber(item_uid))

	result = {result}

	triggerClientEvent(player, "onPlayerItemsReceived", resourceRoot, result)
end

function setItemInUse(item_uid, state)
	local player = client
end

addEvent("onClientSetItemInUse", true)
addEventHandler("onClientSetItemInUse", resourceRoot, setItemInUse)


function createItem(owner_type, owner, name, typee, value1, value2)
	local _, _, uid = exports.cstory_db:queryResult("INSERT INTO items (ownerType, owner, name, type, value1, value2) VALUES (?, ?, ?, ?, ?, ?)", owner_type, owner, name, typee, value1, value2)

	if owner_type == ITEM_OWNER_PLAYER then
		-- wysylamy do clienta
		local target = exports.cstory_core:getPlayerByUid(owner)

		if target then
			sendPlayerItem(target, uid)
		end
	end
end

function removeItem(uid)
	if type(uid) == "number" then
		local result, rows = exports.cstory_db:queryResult("SELECT ownerType FROM items WHERE id = ?", uid)

		if rows == 1 then
			local owner_type = result["ownerType"]
			exports.cstory_db:query("DELETE FROM items WHERE id = ?", uid)
			
			if owner_type == ITEM_OWNER_PLAYER then
				triggerClientEvent(player, "onPlayerItemsDeleted", resourceRoot, uid)
			end
		end
	end
end