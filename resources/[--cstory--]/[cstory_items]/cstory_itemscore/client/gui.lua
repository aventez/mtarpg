local screenWidth, screenHeight = guiGetScreenSize()

local ItemsAction = {"Użyj przedmiotu", "Odłóż przedmiot", "Oferuj najbliższej osobie", "Daj najbliższej osobie", "Zobacz opis przedmiotu", "Cofnij"}

local ItemsGui = {
	display = false,
	action_box = false,
	anim = {
		progress = 1,
		start_time = 0,
		time = 400
	},

	blur = dxCreateShader("files/BlurShader.fx"),
	screen_source = dxCreateScreenSource(screenWidth, screenHeight),

	selected_item = 0,
	selected_action = 0,

	last_mouse2 = 0,

	font = {},

	offset = 0,
	visible = {},

	pos = {
		window = { ["x"] = screenWidth * 0.7432, ["y"] = screenHeight * 0.2425, ["w"] = screenWidth * 0.2479, ["h"] = screenHeight * 0.4842 }
	}
}

function drawItemsGui()
	if not ItemsGui.display and ItemsGui.anim.progress == 1 then return end

	-- animacja
	if ItemsGui.anim.progress < 1 then
		ItemsGui.anim.progress = (getTickCount() - ItemsGui.anim.start_time)/ItemsGui.anim.time
		if ItemsGui.anim.progress >= 1 then
			if not ItemsGui.display then
				destroyElement(ItemsGui.font.s8)
				destroyElement(ItemsGui.font.s12)
				destroyElement(ItemsGui.font.s15)

				ItemsGui.anim.progress = 1
				return
			end
			ItemsGui.anim.progress = 1
		end

		if ItemsGui.display then ItemsGui.pos.window.x, _, _  = interpolateBetween(screenWidth * 1.1, 0, 0, screenWidth - ItemsGui.pos.window.w, 0, 0, ItemsGui.anim.progress, "InQuad")
		else ItemsGui.pos.window.x, _, _ = interpolateBetween(screenWidth - ItemsGui.pos.window.w, 0, 0, screenWidth * 1.1, 0, 0, ItemsGui.anim.progress, "InQuad") end
	end

	--dxSetAspectRatioAdjustmentEnabled(true)
	dxUpdateScreenSource(ItemsGui.screen_source)
    
    dxSetShaderValue(ItemsGui.blur, "ScreenSource", ItemsGui.screen_source);
    dxSetShaderValue(ItemsGui.blur, "BlurStrength", 10);
	dxSetShaderValue(ItemsGui.blur, "UVSize", screenWidth, screenHeight);

	dxDrawImageSection(ItemsGui.pos.window.x, ItemsGui.pos.window.y, ItemsGui.pos.window.w, ItemsGui.pos.window.h, ItemsGui.pos.window.x, ItemsGui.pos.window.y, ItemsGui.pos.window.w, ItemsGui.pos.window.h, ItemsGui.blur)
	dxDrawRectangle(ItemsGui.pos.window.x, ItemsGui.pos.window.y, ItemsGui.pos.window.w, ItemsGui.pos.window.h, tocolor(0, 0, 0, 138), false)
	dxDrawText("Przedmioty", ItemsGui.pos.window.x + (ItemsGui.pos.window.w)/2, ItemsGui.pos.window.y + 20, ItemsGui.pos.window.x + (ItemsGui.pos.window.w)/2, ItemsGui.pos.window.y + 20, tocolor(255, 255, 255, 255), 1, ItemsGui.font.s15, "center", "center")

	-- kolumny
	local column = {}
	column[1] = {ItemsGui.pos.window.x + 8, ItemsGui.pos.window.x + 4 + (ItemsGui.pos.window.w*0.40)}
	dxDrawText("Nazwa", column[1][1], ItemsGui.pos.window.y + 40, column[1][2], ItemsGui.pos.window.y + 50, tocolor(107, 107, 107, 255), 0.8, ItemsGui.font.s8, "left", "center")
	
	column[2] = {column[1][2] + 9, column[1][2] + 9 + (ItemsGui.pos.window.w*0.15)}
	dxDrawText("Właściwość 1", column[2][1], ItemsGui.pos.window.y + 40, column[2][2], ItemsGui.pos.window.y + 50, tocolor(107, 107, 107, 255), 0.8, ItemsGui.font.s8, "right", "center")
	
	column[3] = {column[2][2] + 9, column[2][2] + 9 + (ItemsGui.pos.window.w*0.15)}
	dxDrawText("Właściwość 2", column[3][1], ItemsGui.pos.window.y + 40, column[3][2], ItemsGui.pos.window.y + 50, tocolor(107, 107, 107, 255), 0.8, ItemsGui.font.s8, "right", "center")
	
	column[4] = {column[3][2] + 9, ItemsGui.pos.window.x + ItemsGui.pos.window.w - 4}
	dxDrawText("UID", column[4][1], ItemsGui.pos.window.y + 40, column[4][2], ItemsGui.pos.window.y + 50, tocolor(107, 107, 107, 255), 0.8, ItemsGui.font.s8, "right", "center")

	local i = -ItemsGui.offset
	for x, k in ipairs(getSortedItems()) do
		local v = getItem(k)
		if ItemsGui.selected_item == 0 then ItemsGui.selected_item = k end

		if ItemsGui.pos.window.y + 80 + i*20 < ItemsGui.pos.window.y  + ItemsGui.pos.window.h and not v["hide"] and i >= 0 then
			ItemsGui.visible[k] = true

			if k == ItemsGui.selected_item then 
				dxDrawRectangle(ItemsGui.pos.window.x, ItemsGui.pos.window.y + 60 + i*20, ItemsGui.pos.window.w, 20, tocolor(255, 255, 255, 30), false)
				dxDrawRectangle(ItemsGui.pos.window.x, ItemsGui.pos.window.y + 60 + i*20, 4, 20, tocolor(255, 255, 255, 255), false)

				-- action box
				if ItemsGui.action_box then
					if ItemsGui.selected_action == 0 then ItemsGui.selected_action = 1 end

					local basex = ItemsGui.pos.window.x - ItemsGui.pos.window.w*0.4
					local basey = ItemsGui.pos.window.y + 60 + i*20

					dxDrawImageSection(basex, basey, ItemsGui.pos.window.w*0.4, 120, basex, basey, ItemsGui.pos.window.w*0.4, 120, ItemsGui.blur)
					dxDrawRectangle(basex, basey, ItemsGui.pos.window.w*0.4, 120, tocolor(0, 0, 0, 138), false)

					
					for j=1,6 do
						if ItemsGui.selected_action == j then

							dxDrawRectangle(basex, basey + ((j-1)*20), ItemsGui.pos.window.w*0.4, 20, tocolor(255, 255, 255, 30), false)
						end

						local text = ItemsAction[j]
						if j == 1 and v["used"] then text = "Przestań używać przedmiotu" end
							
						dxDrawText(text, basex + 5, basey + ((j-1)*20), ItemsGui.pos.window.x, basey + 20 + ((j-1)*20), tocolor(255, 255, 255, 255), 0.8, ItemsGui.font.s12, "left", "center")
					end
				end
			else
				-- seperatory
				if not v["group"] then
					dxDrawRectangle(column[1][2] + 4, ItemsGui.pos.window.y + 60 + i*20, 1, 20, tocolor(255, 255, 255, 30), false)
					dxDrawRectangle(column[2][2] + 4, ItemsGui.pos.window.y + 60 + i*20, 1, 20, tocolor(255, 255, 255, 30), false)
					dxDrawRectangle(column[3][2] + 4, ItemsGui.pos.window.y + 60 + i*20, 1, 20, tocolor(255, 255, 255, 30), false)
				end
			end

			local name_color = tocolor(255, 255, 255, 255)
			if v["used"] and not v["group"] then name_color = tocolor(43, 139, 208, 255) end

			if v["group"] then
				dxDrawText("Folder " .. v["name"] .. " (x"..v["groupcount"]..")", column[1][1], ItemsGui.pos.window.y + 60 + i*20, column[1][2], ItemsGui.pos.window.y + 80 + i*20, name_color, 0.8, ItemsGui.font.s12, "left", "center")
			else 
				if v["ingroup"] then dxDrawText("   " .. v["name"], column[1][1], ItemsGui.pos.window.y + 60 + i*20, column[1][2], ItemsGui.pos.window.y + 80 + i*20, name_color, 0.8, ItemsGui.font.s12, "left", "center")
				else dxDrawText(v["name"], column[1][1], ItemsGui.pos.window.y + 60 + i*20, column[1][2], ItemsGui.pos.window.y + 80 + i*20, name_color, 0.8, ItemsGui.font.s12, "left", "center") end
				dxDrawText(v["value1"], column[2][1], ItemsGui.pos.window.y + 60 + i*20, column[2][2], ItemsGui.pos.window.y + 80 + i*20, tocolor(255, 255, 255, 255), 0.8, ItemsGui.font.s12, "right", "center")
				dxDrawText(v["value2"], column[3][1], ItemsGui.pos.window.y + 60 + i*20, column[3][2], ItemsGui.pos.window.y + 80 + i*20, tocolor(255, 255, 255, 255), 0.8, ItemsGui.font.s12, "right", "center")
				dxDrawText(k, column[4][1], ItemsGui.pos.window.y + 60 + i*20, column[4][2], ItemsGui.pos.window.y + 80 + i*20, tocolor(107, 107, 107, 255), 0.8, ItemsGui.font.s12, "right", "center")
			end
			i = i + 1
		else
			if i < 0 then i = i + 1 end
			ItemsGui.visible[k] = false
		end

		
	end
end

function toggleItemsGui()
	if ItemsGui.display and ItemsGui.anim.progress == 1 then
		ItemsGui.display = false
		ItemsGui.action_box = false
		ItemsGui.selected_action = 0
		ItemsGui.anim.progress = 0
		ItemsGui.anim.start_time = getTickCount()

	elseif ItemsGui.anim.progress == 1 and not ItemsGui.display then
		if not hasAnyItem() then return exports.cstory_core:outputCommandError("Nie posiadasz żadnych przedmiotów") end

		ItemsGui.font.s8 = dxCreateFont("files/myriadproregular.ttf", 10)
		ItemsGui.font.s12 = dxCreateFont("files/myriadproregular.ttf", 12)
		ItemsGui.font.s15 = dxCreateFont("files/myriadproregular.ttf", 14)

		-- zamykanie folderow
		for k, v in pairs(getItems()) do
			if v["group"] then
				v["open"] = false
				setItem(k, v)
			elseif v["ingroup"] then
				v["hide"] = true
				setItem(k, v)
			end
		end

		ItemsGui.display = true
		ItemsGui.selected_item = 0
		ItemsGui.offset = 0
		ItemsGui.anim.progress = 0
		ItemsGui.anim.start_time = getTickCount()
	end
end

function keyListener(button, press)
	if not getElementData(localPlayer, "character:char_id") then return end

	if button == "i" and press and not getElementData(localPlayer, "character:chatInput") then
		toggleItemsGui()
	end

	if button == "mouse_wheel_up" and press and ItemsGui.anim.progress == 1 and ItemsGui.display then
		if ItemsGui.action_box then
			if ItemsGui.selected_action > 1 then ItemsGui.selected_action = ItemsGui.selected_action - 1 end
		else
			local prev = false
			local tmp = false
			for x, k in ipairs(getSortedItems()) do
				local v = getItem(k)
				if not v["hide"] then
					if k == ItemsGui.selected_item then
						prev = tmp
					end
					tmp = k
				end
			end

			if prev then
				ItemsGui.selected_item = prev
				onSelectedItemChange()
				if not ItemsGui.visible[prev] then
					ItemsGui.offset = ItemsGui.offset - 1
				end
			end
		end
	elseif button == "mouse_wheel_down" and press and ItemsGui.anim.progress == 1 and ItemsGui.display then
		if ItemsGui.action_box then
			if ItemsGui.selected_action < 6 then ItemsGui.selected_action = ItemsGui.selected_action + 1 end
		else
			local nextt = false
			local tmp = false
			for x, k in ipairs(getSortedItems()) do
				local v = getItem(k)
				if not v["hide"] then
					if tmp == ItemsGui.selected_item then
						nextt = k
					end
					tmp = k
				end
			end

			if nextt then
				ItemsGui.selected_item = nextt
				onSelectedItemChange()
				if not ItemsGui.visible[nextt] then
					ItemsGui.offset = ItemsGui.offset + 1
				end
			end
		end
	end

	if button == "mouse1" and press and ItemsGui.anim.progress == 1 and ItemsGui.display then
		if not ItemsGui.action_box then
			local item = getItem(ItemsGui.selected_item)

			if item and item["group"] then
				-- rozwijanie/zwijanie grup
				if not item["open"] then
					-- rozwijamy
					for k, v in pairs(getItems()) do
						if v["type"] == item["type"] and v["name"] == item["name"] and k ~= ItemsGui.selected_item then
							v["hide"] = false
							setItem(k, v)
						end
					end
					item["open"] = true
					setItem(ItemsGui.selected_item, item)
				else
					-- zwijamy
					for k, v in pairs(getItems()) do
						if v["type"] == item["type"] and v["name"] == item["name"] and k ~= ItemsGui.selected_item then
							v["hide"] = true
							setItem(k, v)
						end
					end
					item["open"] = false
					setItem(ItemsGui.selected_item, item)
				end
			else
				ItemsGui.action_box = true
				ItemsGui.selected_action = 1
			end
		else
			onPlayerItemAction(ItemsGui.selected_item, ItemsGui.selected_action)
			ItemsGui.action_box = false
			ItemsGui.selected_action = 0
		end
		cancelEvent()
	elseif button == "mouse2" and press and ItemsGui.anim.progress == 1 and ItemsGui.display then
		if getTickCount() - ItemsGui.last_mouse2 < 200 then
			toggleItemsGui()
		else
			if ItemsGui.action_box then
				ItemsGui.action_box = false
				ItemsGui.selected_action = 0
			end
		end

		ItemsGui.last_mouse2 = getTickCount()
		cancelEvent()
	end
end

function onSelectedItemChange()
	local item = getItem(ItemsGui.selected_item)
	if item["group"] then
		ItemsGui.selected_item_info = {item["name"], item["type"]}
	else
		ItemsGui.selected_item_info = nil
	end
end

function getSelectedItemInfo()
	return ItemsGui.selected_item_info
end

function getSelectedItem()
	return ItemsGui.selected_item
end

function setSelectedItem(uid)
	ItemsGui.selected_item = uid
end

function onItemAddedToTable()
	if ItemsGui.offset > 0 then
		ItemsGui.offset = ItemsGui.offset + 1
	end
end

function onItemRemovedFromTable()
	if ItemsGui.offset > 0 then
		ItemsGui.offset = ItemsGui.offset - 1
	end
end

addEventHandler("onClientResourceStart", resourceRoot,
    function()
    	addEventHandler("onClientKey", root, keyListener)
    	addEventHandler("onClientRender", root, drawItemsGui)
   	end
)