local playerItems = {}
local sortedItems = {}
local openedGroups = {}

function onPlayerItemsReceived(items)
	for k, v in pairs(playerItems) do
		if v["group"] and v["open"] then
			table.insert(openedGroups, {v["name"], v["type"]})
		end
	end

	local i = 0
	for k, v in ipairs(items) do
		i = i + 1
		local uid = v["id"]

		if type(playerItems[uid]) ~= "table" or playerItems[uid]["group"] then
			playerItems[uid] = {}
			playerItems[uid]["type"] = v["type"]
			playerItems[uid]["name"] = v["name"]
			playerItems[uid]["value1"] = v["value1"]
			playerItems[uid]["value2"] = v["value2"]
			playerItems[uid]["used"] = false

			onItemAddedToTable()
		end
	end

	calculateItemGroups()
	generateSortedList()
end

addEvent("onPlayerItemsReceived", true)
addEventHandler("onPlayerItemsReceived", resourceRoot, onPlayerItemsReceived)

function onPlayerItemsDeleted(uid)
	for k, v in pairs(playerItems) do
		if v["group"] and v["open"] then
			table.insert(openedGroups, {v["name"], v["type"]})
		end
	end

	if type(playerItems[uid]) == "table" then
		if getSelectedItem() == uid then
			-- to byl zaznaczony item
			local x, y, z = 0
			for k,v in pairs(playerItems) do
				if y > 0 and z == 0 then z = k end
				if k == uid then y = x end
				x = k
			end

			if z > 0 then setSelectedItem(z)
			elseif y > 0 then setSelectedItem(y)
			else toggleItemsGui() end
		end

		playerItems[uid] = nil
	end

	calculateItemGroups()
	generateSortedList()
end

addEvent("onPlayerItemsDeleted", true)
addEventHandler("onPlayerItemsDeleted", resourceRoot, onPlayerItemsDeleted)

function getSortedItems()
	return sortedItems
end

function getItems()
	return playerItems
end

function getItem(uid)
	if type(uid) == "number" then
		if type(playerItems[uid]) == "table" then
			return playerItems[uid]
		end
	end

	return false
end

function setItem(uid, item)
	if type(uid) == "number" then
		if type(playerItems[uid]) == "table" then
			playerItems[uid] = item
		end
	end
end

function hasAnyItem()
	for k,v in pairs(playerItems) do
		return true
	end

	return false
end

function calculateItemGroups()
	local i = 0

	-- usuwamy dummy przedmioty dla nagłówków grup
	for k, v in pairs(playerItems) do
		if v["group"] then
			playerItems[k] = nil
		end

		if v["ingroup"] then
			playerItems[k]["ingroup"] = false
			playerItems[k]["hide"] = false
		end
	end

	local folders = {}
	for k, v in pairs(playerItems) do
		if not playerItems[k]["ingroup"] then
			for k2, v2 in pairs(playerItems) do
				if v["name"] == v2["name"] and v["type"] == v2["type"] then
					i = i + 1
				end
			end

			if i >= 5 then
				-- robimy folder
				
				for k2, v2 in pairs(playerItems) do
					if v["name"] == v2["name"] and v["type"] == v2["type"] then
						if wasGroupOpened(v2["name"], v2["type"]) then
							playerItems[k2]["hide"] = false
						else
							playerItems[k2]["hide"] = true
						end

						playerItems[k2]["ingroup"] = true
					end
				end

				-- tworzymy dummy przedmiot dla nagłowka folderu
				local item = {}
				item["group"] = true
				item["groupcount"] = i
				item["name"] = playerItems[k]["name"]
				item["type"] = playerItems[k]["type"]

				if wasGroupOpened(item["name"], item["type"]) then
					item["open"] = true
				end

				table.insert(folders, item)
			else
				playerItems[k]["group"] = false
			end

			i = 0
		end
	end

	for k, v in ipairs(folders) do
		table.insert(playerItems, v)
	end

	-- jesli mial folder zaznaczony to musimy znalezc ten folder na nowo i mu zaznaczyc
	local item = getSelectedItemInfo()
	if item then
		for k, v in pairs(playerItems) do
			if v["group"] and v["name"] == item[1] and v["type"] == item[2] then
				setSelectedItem(k)
			end
		end
	end

	openedGroups = {}
end

function wasGroupOpened(name, type)
	for s, z in ipairs(openedGroups) do
		if z[1] == name and tonumber(z[2]) == tonumber(type) then 
			return true 
		end
	end

	return false
end


function generateSortedList()
	sortedItems = {}

	for k, v in pairs(playerItems) do
		if not v["ingroup"] and not v["group"] then
			table.insert(sortedItems, k)
		end
	end

	for k, v in pairs(playerItems) do
		if v["group"] then
			table.insert(sortedItems, k)
		end
	end

	for k, v in ipairs(sortedItems) do
		if playerItems[v]["group"] then
			-- jesli natkniemy sie na grupe wtedy w to miejsce wrzyucamy wszystkie przedmioty nalezace do tej grupy
			for k2, v2 in pairs(playerItems) do
				if v2["ingroup"] and playerItems[v]["name"] == v2["name"] and playerItems[v]["type"] == v2["type"] then
					table.insert(sortedItems, k+1, k2)
				end
			end
		end
	end
end

function onPlayerItemAction(item_uid, action)
	if action == 1 then
		-- uzywanie przedmiotu
		if playerItems[item_uid]["used"] then
			setItemInUse(item_uid, false)
		else
			setItemInUse(item_uid, true)
		end
	end
end

function setItemInUse(item_uid, state)
	if type(item_uid) == "number" then
		if state then
			playerItems[item_uid]["used"] = true
			triggerServerEvent("onClientSetItemInUse", resourceRoot, item_uid, state)
		else
			playerItems[item_uid]["used"] = false
			triggerServerEvent("onClientSetItemInUse", resourceRoot, item_uid, state)
		end
	end
end

addEventHandler("onClientResourceStart", resourceRoot,
    function()
    	triggerServerEvent("onClientRequestItems", resourceRoot)
   	end
)