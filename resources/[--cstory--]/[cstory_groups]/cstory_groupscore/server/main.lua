GroupsManager = {}
Groups = {}
PlayerGroups = {}

function GroupsManager.load(data)
	data = data or ""
	local query = string.format("SELECT * FROM groups%s",data)
	local groups, rows = exports.cstory_db:queryResult(query)

	if not rows then return end
	if rows == 1 then groups = {groups} end
	
	for k,v in pairs(groups) do
		Groups[v['id']] = {
			name = v['name'],
			type = v['type'],
			cash = v['cash'],
			owner = v['owner'],
			color = {exports.cstory_core:hex2rgb(v['color'])},
			tag = v['tag'],
			flags = v['flags'],
			players = {}
		}
	end

	if string.len(data) == 0 then outputDebugString(string.format("[cstory_groups] Wczytano %d grup.", rows)) end
end
addEventHandler("onResourceStart", resourceRoot, function ()
	GroupsManager.load()

	for k,v in pairs(getElementsByType("player")) do
		if getElementData(v, "character:char_id") > 0 then
			GroupsManager.loadPlayerGroups(v)
		end
	end
end)

function GroupsManager.create(name)
	local row, rows, insertid = exports.cstory_db:queryResult("INSERT INTO groups (name) VALUES (?)", name)

	if insertid > 0 then
		GroupsManager.load(string.format(" WHERE id = %d", insertid))
		return insertid
	end

	return false
end

function GroupsManager.loadPlayerGroups(player)
	if player then source = player end
	if getElementType(source) ~= "player" then return end

	local uid = getElementData(source, "character:char_id")
	if uid < 1 then return end

	local pgroups, rows = exports.cstory_db:queryResult("SELECT * FROM group_members WHERE charId = ?", uid)

	PlayerGroups[uid] = {}

	if not rows then return end
	if rows == 1 then pgroups = {pgroups} end
	
	for k,v in pairs(pgroups) do
		table.insert(PlayerGroups[uid], {
			id = v['id'],
			group_uid = v['groupId'],
			perm = v['perm'],
			title = v['title'],
			skin = v['skin']
		})

		table.insert(Groups[tonumber(v['groupId'])]['players'], source)
	end

	outputDebugString(string.format("[cstory_groups] Wczytano %d grup gracza %s.", rows, getPlayerName(source)))
end
addEventHandler("hook_onPlayerSpawnedInGame", root, GroupsManager.loadPlayerGroups)

function GroupsManager.purgePlayerGroups()
	if getElementType(source) ~= "player" then return end

	local uid = getElementData(source, "character:char_id")
	if uid < 1 then return end

	for k,v in ipairs(PlayerGroups[uid]) do
		for i,j in ipairs(Groups[tonumber(v['group_uid'])]['players']) do
			if j == source then
				table.remove(Groups[v['group_uid']]['players'], i)
				break
			end
		end
	end

	PlayerGroups[uid] = nil

	outputDebugString(string.format("[cstory_groups] Wyczyszczono grupy gracza %s.", getPlayerName(source)))
end
addEventHandler("hook_onPlayerSaveAfterQuit", root, GroupsManager.purgePlayerGroups)