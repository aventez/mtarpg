function aGroupCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") or not getElementData(source, "character:adminDuty") or getElementData(source, "global:member_admin_level") < 2 then return end

	local arg = {...}

	local sub = arg[1] or ""

	if sub == "stworz" then
		table.remove(arg, 1)
		name = table.concat(arg, ' ') or ""

		if string.len(name) == 0 then return exports.cstory_core:outputCommandError("Tip: /(ag)rupa stworz [nazwa]", source) end

		local guid = GroupsManager.create(name)
		if guid > 0 then
			exports.cstory_core:outputInfoBox(string.format("Pomyślnie utworzono grupę %s (UID: %d)", name, guid), source)
		end
	elseif sub == "usun" then
		uid = tonumber(arg[2]) or 0

		if uid == 0 then return exports.cstory_core:outputCommandError("Tip: /(ag)rupa usun [uid]", source) end
	else
		exports.cstory_core:outputCommandError("Tip: /(ag)rupa stworz/usun/typ/kolor/tag/flagi/lider/zapros/wypros", source)
	end
end
addCommandHandler("agrupa", aGroupCommand)
addCommandHandler("ag", aGroupCommand)