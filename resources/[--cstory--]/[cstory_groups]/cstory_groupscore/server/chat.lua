function groupChat(group_slot, msg)
	if getElementType(source) ~= "player" then return end

	local uid = getElementData(source, "character:char_id")
	local group_uid = 0
	for k,v in ipairs(PlayerGroups[uid]) do

		if k == group_slot then 
			group_uid = v.group_uid
			break
		end
	end

	if group_uid == 0 then return end
	local formatted = string.format(" %s]: (( %s [%d]: %s ))", Groups[group_uid].tag, getElementData(source, "character:char_name"), getElementData(source, "character:id"), msg)
	for i,target in ipairs(Groups[group_uid].players) do
		local slot = 0
		for k,v in ipairs(PlayerGroups[getElementData(target, "character:char_id")]) do
			if v.group_uid == group_uid then 
				slot = k
				break
			end
		end
		outputChatBox(string.format("[@%d", slot) .. formatted, target, Groups[group_uid].color[1], Groups[group_uid].color[2], Groups[group_uid].color[3])
	end
	
end
addEventHandler("onPlayerGroupChatSend", root, groupChat)