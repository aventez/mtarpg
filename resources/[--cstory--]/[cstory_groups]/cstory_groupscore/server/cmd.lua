function groupCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") then return end

	local arg = {...}

	local sub = arg[1] or ""

	if sub == "stworz" then
		table.remove(arg, 1)
		name = table.concat(arg, ' ') or ""

		if string.len(name) == 0 then return exports.cstory_core:outputCommandError("Tip: /(ag)rupa stworz [nazwa]", source) end

		local guid = GroupsManager.create(name)
		if guid > 0 then
			exports.cstory_core:outputInfoBox(string.format("Pomyślnie utworzono grupę %s (UID: %d)", name, guid), source)
		end
	else
		-- tu wyswietlamy okno grup
		local groups = {}
		local uid = getElementData(source, "character:char_id")

		local count = 0
		for k,v in ipairs(PlayerGroups[uid]) do
			table.insert(groups, {
				uid = v["group_uid"],
				name = Groups[v["group_uid"]]["name"]
			})
			count = count + 1
		end
		if count > 0 then
			triggerClientEvent(source, "onPlayerGroupsWindowRequest", resourceRoot, groups)
		else
			exports.cstory_core:outputInfoBox("Nie jesteś członkiem żadnej grupy", source)
		end
	end
end
addCommandHandler("grupa", groupCommand)
addCommandHandler("g", groupCommand)


function playerRequestFromGroupWindow(group_uid, action)
	exports.cstory_core:outputInfoBox(string.format("Teraz powinno wyświetlić się '%s' grupy o uid %d", action, group_uid), client)
end
addEvent("onPlayerRequestFromGroupWindow", true)
addEventHandler("onPlayerRequestFromGroupWindow", resourceRoot, playerRequestFromGroupWindow)
