local screenW, screenH = guiGetScreenSize()
local GroupsWindow = {
	enabled = false,
	visible = false,
	data = nil,

	size = {
		width = 600,
		height = 30
	},

	row = nil,
	anim = {}
}
local anims = {progress = {}, position = {}}

GroupsWindow.row = {screenW/2-(GroupsWindow.size.width/2),screenH*0.36}

function showGroupsWindow(data)
	if(GroupsWindow.enabled) then return end

	GroupsWindow.data = data

	addEventHandler("onClientKey", root, keyListener)
	addEventHandler("onClientClick", root, groupsWindowClick)
	addEventHandler("onClientRender", root, renderGroupsWindow)
	showCursor(true)

	setCursorPosition (screenW/2, screenH/2)

	GroupsWindow.enabled = true
	GroupsWindow.visible = false
	GroupsWindow.anim.start = getTickCount()
	GroupsWindow.anim.type = "show"
	GroupsWindow.row_tmp = GroupsWindow.row
end

addEvent("onPlayerGroupsWindowRequest", true)
addEventHandler("onPlayerGroupsWindowRequest", resourceRoot, showGroupsWindow)

function hideGroupsWindow()
	GroupsWindow.visible = false
	GroupsWindow.anim.type = "hide"
	GroupsWindow.anim.start = getTickCount()
	showCursor(false)
	cancelEvent()
end

function keyListener(button, press)
	if button == "escape" and press and GroupsWindow.visible then
		hideGroupsWindow()
	end
end

function renderGroupsWindow()

	local summ_progress = 0

	for k,v in pairs(GroupsWindow.data) do
		GroupsWindow.row = GroupsWindow.row_tmp

		if not GroupsWindow.visible then
			if GroupsWindow.anim.type == "show" then anims.progress[k] = (getTickCount() - GroupsWindow.anim.start) / (1000 + ((k-1)*200))
			elseif GroupsWindow.anim.type == "hide" then anims.progress[k] = (getTickCount() - GroupsWindow.anim.start) / (500 + ((k-1)*100)) end

			if anims.progress[k] > 1 then anims.progress[k] = 1 end
			summ_progress = summ_progress + anims.progress[k]

			local x, y, z
			if GroupsWindow.anim.type == "show" then
				x, y, z = interpolateBetween(-600, GroupsWindow.row[2], 0, GroupsWindow.row[1], GroupsWindow.row[2], 0, anims.progress[k], "OutBack")
			elseif GroupsWindow.anim.type == "hide" then
				x, y, z = interpolateBetween(GroupsWindow.row[1], GroupsWindow.row[2], 0, -600, GroupsWindow.row[2], 0, anims.progress[k], "InBack")
			end
			GroupsWindow.row = {x, GroupsWindow.row_tmp[2]}
		end

		local ypadding = (k-1) * (GroupsWindow.size.height + 5)
		-- base
	    dxDrawRectangle(GroupsWindow.row[1], ypadding + GroupsWindow.row[2], GroupsWindow.size.width, GroupsWindow.size.height, tocolor(1, 0, 0, 193), false)
	    -- base for group slot
	    dxDrawRectangle(GroupsWindow.row[1] + 10, ypadding + GroupsWindow.row[2], 25, GroupsWindow.size.height, tocolor(0, 142, 243, 186), false)

	    -- slot number
	    dxDrawText(k, GroupsWindow.row[1] + 10 - 1, ypadding + GroupsWindow.row[2] - 1, GroupsWindow.row[1] + 10 + 25 - 1, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height - 1, tocolor(0, 0, 0, 255), 1.00, "default-bold", "center", "center", false, false, false, false, true)
	    dxDrawText(k, GroupsWindow.row[1] + 10 + 1, ypadding + GroupsWindow.row[2] - 1, GroupsWindow.row[1] + 10 + 25 + 1, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height - 1, tocolor(0, 0, 0, 255), 1.00, "default-bold", "center", "center", false, false, false, false, true)
	    dxDrawText(k, GroupsWindow.row[1] + 10 - 1, ypadding + GroupsWindow.row[2] + 1, GroupsWindow.row[1] + 10 + 25 - 1, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height + 1, tocolor(0, 0, 0, 255), 1.00, "default-bold", "center", "center", false, false, false, false, true)
	    dxDrawText(k, GroupsWindow.row[1] + 10 + 1, ypadding + GroupsWindow.row[2] + 1, GroupsWindow.row[1] + 10 + 25 + 1, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height + 1, tocolor(0, 0, 0, 255), 1.00, "default-bold", "center", "center", false, false, false, false, true)

	    dxDrawText(k, GroupsWindow.row[1] + 10, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 10 + 25, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, false, false, true)

	    -- group name
	    dxDrawText(string.format("%s (UID: %d)", v["name"], v["uid"]), GroupsWindow.row[1] + 40 + 1, ypadding + GroupsWindow.row[2] + 1, GroupsWindow.row[1] + 40 + 275 + 1, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height + 1, tocolor(0, 0, 0, 255), 1.00, "default-bold", "left", "center", true, false, false, false, true)
	    dxDrawText(string.format("%s (UID: %d)", v["name"], v["uid"]), GroupsWindow.row[1] + 40, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 40 + 275, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height, tocolor(255, 255, 255, 255), 1.00, "default-bold", "left", "center", true, false, false, false, true)

	    -- action buttons
	    local color = tocolor(255, 255, 255, 255)
	    if GroupsWindow.visible and isMouseIn(GroupsWindow.row[1] + 320, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 320 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height) then
	    	dxDrawRectangle(GroupsWindow.row[1] + 320, ypadding + GroupsWindow.row[2], 65, GroupsWindow.size.height, tocolor(0, 0, 0, 96), false)
	    	color = tocolor(0, 142, 243, 186)
	    end
	    dxDrawText("Służba", GroupsWindow.row[1] + 320, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 320 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height, color, 1.00, "arial", "center", "center", false, false, false, false, true)
	    color = tocolor(255, 255, 255, 255)

	    if GroupsWindow.visible and isMouseIn(GroupsWindow.row[1] + 390, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 390 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height) then
	    	dxDrawRectangle(GroupsWindow.row[1] + 390, ypadding + GroupsWindow.row[2], 65, GroupsWindow.size.height, tocolor(0, 0, 0, 96), false)
	    	color = tocolor(0, 142, 243, 186)
	    end
	    dxDrawText("Info", GroupsWindow.row[1] + 390, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 390 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height, color, 1.00, "arial", "center", "center", false, false, false, false, true)
		color = tocolor(255, 255, 255, 255)

	    if GroupsWindow.visible and isMouseIn(GroupsWindow.row[1] + 460, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 460 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height) then
	    	dxDrawRectangle(GroupsWindow.row[1] + 460, ypadding + GroupsWindow.row[2], 65, GroupsWindow.size.height, tocolor(0, 0, 0, 96), false)
	    	color = tocolor(0, 142, 243, 186)
	    end
	    dxDrawText("Pojazdy", GroupsWindow.row[1] + 460, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 460 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height, color, 1.00, "arial", "center", "center", false, false, false, false, true)
   		color = tocolor(255, 255, 255, 255)

	   	if GroupsWindow.visible and isMouseIn(GroupsWindow.row[1] + 530, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 530 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height) then
	   		dxDrawRectangle(GroupsWindow.row[1] + 530, ypadding + GroupsWindow.row[2], 65, GroupsWindow.size.height, tocolor(0, 0, 0, 96), false)
	   		color = tocolor(0, 142, 243, 186)
	   	end
	   	dxDrawText("Online", GroupsWindow.row[1] + 530, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 530 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height, color, 1.00, "arial", "center", "center", false, false, false, false, true)
	end

	if summ_progress >= #GroupsWindow.data and GroupsWindow.anim.type == "show" then
		GroupsWindow.visible = true
	elseif summ_progress >= #GroupsWindow.data and GroupsWindow.anim.type == "hide" then
		removeEventHandler("onClientKey", root, keyListener)
		removeEventHandler("onClientClick", root, groupsWindowClick)
		removeEventHandler("onClientRender", root, renderGroupsWindow)
		GroupsWindow.enabled = false
		GroupsWindow.row = GroupsWindow.row_tmp
	end
end

function isMouseIn(psx,psy,pssx,pssy)
    if not isCursorShowing() then return false end
    local absoluteX, absoluteY = getCursorPosition()
    absoluteX = absoluteX * screenW
    absoluteY = absoluteY * screenH
    
    if absoluteX >= psx and absoluteX <= pssx and absoluteY >= psy and absoluteY <= pssy then return true end

    return false
end

function groupsWindowClick(btn,state)
    if btn=="left" and state=="down" and GroupsWindow.visible then
    	for k,v in pairs(GroupsWindow.data) do
    		local ypadding = (k-1) * (GroupsWindow.size.height + 5)
    		if isMouseIn(GroupsWindow.row[1] + 320, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 320 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height) then
	        	triggerServerEvent("onPlayerRequestFromGroupWindow", resourceRoot, v.uid, "duty")
	        	hideGroupsWindow()
	        elseif isMouseIn(GroupsWindow.row[1] + 390, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 390 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height) then
	        	triggerServerEvent("onPlayerRequestFromGroupWindow", resourceRoot, v.uid, "info")
	        	hideGroupsWindow()
	        elseif isMouseIn(GroupsWindow.row[1] + 460, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 460 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height) then
	        	triggerServerEvent("onPlayerRequestFromGroupWindow", resourceRoot, v.uid, "vehicles")
	        	hideGroupsWindow()
	        elseif isMouseIn(GroupsWindow.row[1] + 530, ypadding + GroupsWindow.row[2], GroupsWindow.row[1] + 530 + 65, ypadding + GroupsWindow.row[2] + GroupsWindow.size.height) then
	        	triggerServerEvent("onPlayerRequestFromGroupWindow", resourceRoot, v.uid, "online")
	        	hideGroupsWindow()
	        end
    	end
    end
end