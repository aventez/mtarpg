local adminsList = {
    gridlist = {},
    window = {}
}

function adminsListCommand(cmd, ...)
    if not getElementData(localPlayer, "character:char_id") then return end

    local admins = 0
    for k, v in ipairs(getElementsByType("player")) do
        if getElementData(v, "character:adminDuty") then admins = admins + 1 end
    end

    if admins == 0 then return exports.cstory_core:outputCommandError("Aktualnie nie ma żadnych administratorów online") end

    local screenW, screenH = guiGetScreenSize()
    adminsList.window[1] = guiCreateWindow((screenW - 297) / 2, (screenH - 385) / 2, 297, 385, "Administratorzy online", false)
    guiWindowSetSizable(adminsList.window[1], false)

    adminsList.gridlist[1] = guiCreateGridList(10, 27, 277, 348, false, adminsList.window[1])
    guiGridListSetSortingEnabled(adminsList.gridlist[1], false)
    guiGridListSetScrollBars(adminsList.gridlist[1], false, false)
    guiGridListAddColumn(adminsList.gridlist[1], "Nick", 0.4)
    guiGridListAddColumn(adminsList.gridlist[1], "ID", 0.2)

    for k, v in ipairs(getElementsByType("player")) do
        if getElementData(v, "character:adminDuty") then 
            guiGridListAddRow(adminsList.gridlist[1])
            guiGridListSetItemText(adminsList.gridlist[1], 0, 1, getElementData(v, "character:char_name"), false, false)
            guiGridListSetItemText(adminsList.gridlist[1], 0, 2, getElementData(v, "character:id"), false, false) 
        end
    end

    showCursor(true)

    -- zamykanie 
    local w = guiGetSize(adminsList.window[1], false)
    local width = dxGetTextWidth("zamknij", 1, "default")
    local label = guiCreateLabel("right" == "left" and 5 or w - 5 - width, 2, width, 15, "zamknij", false, adminsList.window[1])
    
    guiLabelSetColor(label, 160, 160, 160)
    guiLabelSetHorizontalAlign(label, "center", false)
    guiSetProperty(label, "ClippedByParent", "False")
    guiSetProperty(label, "AlwaysOnTop", "True")

    addEventHandler("onClientGUIClick", label, 
        function(button, state)
            if button == "left" and state == "up" then
                -- back
                destroyElement(adminsList.window[1])
                showCursor(false)
            end
        end, 
    false)

    addEventHandler("onClientMouseEnter", label, function() guiLabelSetColor(label, 255, 69, 59) end, false)
    addEventHandler("onClientMouseLeave", label, function() guiLabelSetColor(label, 160, 160, 160) end, false)
    --- zamykanie
end
addCommandHandler("admins", adminsListCommand)

function aposCommand(cmd, ...)
    if not getElementData(localPlayer, "character:char_id") or not getElementData(localPlayer, "character:adminDuty") then return end

    local x, y, z = getElementPosition(localPlayer)

    outputConsole(string.format("%.2f, %.2f, %.2f", x, y, z))
end
addCommandHandler("apos", aposCommand)

addCommandHandler("adev",
    function (source, cmd, ...)
        if not getElementData(localPlayer, "character:char_id") or not getElementData(localPlayer, "character:adminDuty") or getElementData(localPlayer, "global:member_admin_level") < 2 then return end
        setDevelopmentMode(not getDevelopmentMode())

        if getDevelopmentMode() then exports.cstory_core:showInfoBox("Debug mode został włączony")
        else exports.cstory_core:showInfoBox("Debug mode został wyłączony") end
    end
)