function hasAdminRight(player, right_name)
	local nodes = {}
	local sql = ""
	local x = 0
	for w in (right_name .. "."):gmatch("([^.]*).") do 
	    table.insert(nodes, w)
	    x = x + 1
	end

	if x > 1 then
		table.remove(nodes, x)

		local tmp = {}
		for n, w in ipairs(nodes) do
			local node = ""
	    	for i = 1, n do
	    		node = node .. nodes[i] .. "."
	    	end
	    	table.insert(tmp, node .. "*")
		end

		table.insert(tmp, right_name)
		nodes = tmp

		sql = "name IN ("
		for k, v in ipairs(nodes) do
			sql = sql .. string.format("'%s',", v)
		end
		sql = string.sub(sql, 1, string.len(sql)-1) .. ")"
	else
		sql = "name LIKE '" .. right_name .. "%'"
	end

	for k, v in ipairs(nodes) do
		print(v)
	end

	local result, rows = exports.cstory_db:queryResult(string.format("SELECT true as has FROM admin_rights WHERE globalId = ? AND (%s)", sql), getElementData(player, "global:member_id"))
	if rows > 0 then return true end

	return false
end

-- samo aprzedmiot - sprwadza czy ma w ogole jakis child z aprzedmiot np: aprzedmiot.* albo aprzedmiot.stworz