function aItemCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") or not getElementData(source, "character:adminDuty") or getElementData(source, "global:member_admin_level") < 2 then return end

	local arg = {...}

	local sub = arg[1] or ""

	if sub == "stworz" then
		owner_type = tonumber(arg[2]) or 0
		owner = tonumber(arg[3]) or 0
		name = arg[4] or ""
		typee = tonumber(arg[5]) or 0
		value1 = tonumber(arg[6]) or 0
		value2 = tonumber(arg[7]) or 0

		if owner_type == 0 or owner == 0 or typee == 0 or string.len(name) == 0 then return exports.cstory_core:outputCommandError("Tip: /aitem stworz [owner_type] [owner] [name] [type] [v1] [v2]", source) end

		exports.cstory_itemscore:createItem(owner_type, owner, name, typee, value1, value2)

	elseif sub == "usun" then
		uid = tonumber(arg[2]) or 0

		if uid == 0 then return exports.cstory_core:outputCommandError("Tip: /aitem usun [uid]", source) end
	end
end
addCommandHandler("aitem", aItemCommand)

function aGiveCashCommand(source, cmd, player, amount)
	if not getElementData(source, "character:char_id") or not getElementData(source, "character:adminDuty") or getElementData(source, "global:member_admin_level") < 2 then return end

	player = tonumber(player) or 0
	amount = tonumber(amount) or 0

	if player == 0 then return exports.cstory_core:outputCommandError("Tip: /agivecash [id gracza] [ilość]", source) end
	player = exports.cstory_id:getPlayerByID(player)
	if not isElement(player) then return exports.cstory_core:outputCommandError("Tip: /agivecash [id gracza] [ilość]", source) end
	if amount == 0 then return exports.cstory_core:outputCommandError("Tip: /agivecash [id gracza] [ilość]", source) end

	exports.cstory_core:giveMoney(player, amount)

	exports.cstory_core:outputInfoBox(string.format("Otrzymałeś $%d gotówki od administratora %s", amount, getElementData(source, "character:char_name")), player)
	if player ~= source then exports.cstory_core:outputInfoBox(string.format("Dałeś $%d gotówki graczowi %s (ID: %d;UID: %d)", amount, getElementData(player, "character:char_name"), getElementData(player, "character:id"), getElementData(player, "character:char_id")), source) end
end
addCommandHandler("agivecash", aGiveCashCommand)

function aGiveScoreCommand(source, cmd, player, amount)
	if not getElementData(source, "character:char_id") or not getElementData(source, "character:adminDuty") or getElementData(source, "global:member_admin_level") < 2 then return end

	player = tonumber(player) or 0
	amount = tonumber(amount) or 0

	if player == 0 then return exports.cstory_core:outputCommandError("Tip: /agivescore [id gracza] [ilość]", source) end
	player = exports.cstory_id:getPlayerByID(player)
	if not isElement(player) then return exports.cstory_core:outputCommandError("Tip: /agivescore [id gracza] [ilość]", source) end
	if amount == 0 then return exports.cstory_core:outputCommandError("Tip: /agivescore [id gracza] [ilość]", source) end

	exports.cstory_core:giveScore(player, amount)

	exports.cstory_core:outputInfoBox(string.format("Otrzymałeś %d gameScore od administratora %s", amount, getElementData(source, "character:char_name")), player)
	if player ~= source then exports.cstory_core:outputInfoBox(string.format("Dałeś %d gameScore graczowi %s (ID: %d;UID: %d)", amount, getElementData(player, "character:char_name"), getElementData(player, "character:id"), getElementData(player, "character:char_id")), source) end
end
addCommandHandler("agivescore", aGiveScoreCommand)