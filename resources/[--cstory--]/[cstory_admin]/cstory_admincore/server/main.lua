function onPlayerFirstSpawned()
	if getElementData(source, "global:member_admin_level") > 0 then outputChatBox(string.format("Na tym koncie posiadasz uprawnienia administratora %d poziomu", getElementData(source, "global:member_admin_level")), source, 255, 0, 0) end
end
addEventHandler("hook_onPlayerSpawnedInGame", root, onPlayerFirstSpawned)

function adminDutyCommand(source, cmd, ...)
	if not getElementData(source, "character:char_id") or getElementData(source, "global:member_admin_level") == 0 then return end

	if getElementData(source, "character:adminDuty") then
		-- schodzimy z duty
		removeElementData(source, "character:adminDuty")

		setPlayerName(source, string.gsub(getElementData(source, "global:member_name"), " ", "_"))
		local tmp = getElementData(source, "character:char_name")
		setElementData(source, "character:char_name", getElementData(source, "global:member_name"))
		setElementData(source, "global:member_name", tmp)

		outputChatBox("Zszedłeś z służby administratora", source, 255, 0, 0)
	else
		setElementData(source, "character:adminDuty", true)
		setPlayerName(source, getElementData(source, "global:member_name"))

		local tmp = getElementData(source, "character:char_name")
		setElementData(source, "character:char_name", getElementData(source, "global:member_name"))
		setElementData(source, "global:member_name", tmp)

		outputChatBox("Wszedłeś na służbę administratora", source, 255, 0, 0)
	end
end
addCommandHandler("aduty", adminDutyCommand)